package mx.gnkl.tracking.api.controller;

import mx.gnkl.tracking.api.model.TbUsuarios;
import mx.gnkl.tracking.api.persistence.jpa.UserRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Controller
@RequestMapping(value = "/login")
public class LoginController {
    
    @Autowired
    private UserRepository usuarioRepository;
    

    @Autowired()
    private UserDetailsService userDetailsService; 
    
    /*@RequestMapping(method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Tu usuario y password es inválido.");

        if (logout != null)
            model.addAttribute("message", "Has terminado sesión.");

        return "login";
    }*/
    
@ResponseBody    
@RequestMapping(value = "/android_login", method = { RequestMethod.GET, RequestMethod.POST })
public TbUsuarios loginAndroid(@RequestParam() String username, @RequestParam() String password) {
    boolean isValidUser = true;
    if(username!=null && !username.isEmpty() && password!=null){
        
        UserDetails userDetails = userDetailsService.loadUserByUsername (username);
        if(userDetails!=null){
            //TbUsuarios user = usuarioRepository.findDataByUsuario(username);
//            String usuario =user.getNombre()+" "+user.getApellidoPat()+" "+user.getApellidoMat();
//            SecurityUserBean securityUser = new SecurityUserBean(userDetails.getUsername(), userDetails.getPassword (), userDetails.getAuthorities(), user.getIdUsu(), user.getEmail(),usuario.replace("null", "").toUpperCase(),"");
//            Authentication auth = new UsernamePasswordAuthenticationToken (securityUser,null,userDetails.getAuthorities ());
//            SecurityContextHolder.getContext().setAuthentication(auth);  
            BCryptPasswordEncoder pe= new BCryptPasswordEncoder();
            if (userDetails!=null && userDetails.getPassword()!=null && !userDetails.getPassword().isEmpty() && pe.matches(password, userDetails.getPassword())) {
                // Encode new password and store it
                System.out.println("matches");
                return usuarioRepository.findDataByUsuario(userDetails.getUsername());
            }        
            
        }
    }
    return new TbUsuarios();
}
    
    
}
