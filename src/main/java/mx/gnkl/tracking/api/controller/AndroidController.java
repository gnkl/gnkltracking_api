/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.controller;

import mx.gnkl.tracking.api.component.ApplicationMailer;
import mx.gnkl.tracking.api.model.TbBitacora;
import mx.gnkl.tracking.api.model.TbBitacoraHistorico;
import mx.gnkl.tracking.api.model.TbBitacoraStatus;
import mx.gnkl.tracking.api.model.TbDomainConfig;
import mx.gnkl.tracking.api.model.TbRuta;
import mx.gnkl.tracking.api.model.TbRutaInterrupcion;
import mx.gnkl.tracking.api.model.TbUsuarios;
import mx.gnkl.tracking.api.persistence.dao.BitacoraDao;
import mx.gnkl.tracking.api.persistence.jpa.BitacoraHistoricoRepository;
import mx.gnkl.tracking.api.persistence.jpa.BitacoraRepository;
import mx.gnkl.tracking.api.persistence.jpa.DomainConfigRepository;
import mx.gnkl.tracking.api.persistence.jpa.UserRepository;
import mx.gnkl.tracking.api.service.BitacoraService;
import mx.gnkl.tracking.api.service.RutaService;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import mx.com.gnkl.novartis.component.ApplicationMailer;
//import mx.com.gnkl.novartis.repository.BitacoraDao;
//import mx.com.gnkl.novartis.service.BitacoraService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import javax.annotation.Resource;

/**
 *
 * @author jmejia
 */
@Controller
@RequestMapping(value = "/android")
public class AndroidController {
    
     private static final Logger logger = Logger.getLogger(AndroidController.class);
    
    @Autowired
    private BitacoraRepository bitacoraRepository;    
    
    @Autowired
    private BitacoraDao bitacoraDao;
    
    @Autowired
    private BitacoraService bitacoraService;
    
    @Autowired
    private BitacoraHistoricoRepository bitacoraHistorico;  
    
    @Autowired
    private DomainConfigRepository configRepository;
    
    @Autowired
    private RutaService rutaService;
    
    @Autowired
    private UserRepository usuarioRepository;    

    @Resource
    private ApplicationMailer mailer;
    
    SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    SimpleDateFormat hr1 = new SimpleDateFormat("HH:mm:ss");    

    @ResponseBody
    @RequestMapping(value = "/list_bitacora_tipo_operador", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String,Object>> listAll(@RequestParam() Integer iduser, @RequestParam() Integer tipo) {
        List<Map<String,Object>> listBitacora = new ArrayList<Map<String,Object>>();
        //SecurityUserBean securityUser = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(iduser!=null && tipo!=null && iduser>0){
            listBitacora = bitacoraDao.getAllBitacoraByUsuarioEnvio(iduser, tipo);
        }
        return listBitacora;
    }    
    

    @ResponseBody
    @RequestMapping(value = "/get_bitacora", method = RequestMethod.GET, produces = "application/json")
    public TbBitacora getBitacoraById(@RequestParam() Integer idbitacora) {
        if(idbitacora!=null){
            return bitacoraRepository.findOne(idbitacora);
        }
        return new TbBitacora();
    }        

    @ResponseBody
    @RequestMapping(value = "/current_status", method = RequestMethod.GET, produces = "application/json")
    public Map<String,Object> getCurrentStatusBitacoraById(@RequestParam() Integer idbitacora) {
        if(idbitacora!=null){
            Map<String,Object> map = new HashMap<String,Object>();
            TbBitacora bitacora = bitacoraRepository.findOne(idbitacora);
            if(bitacora != null){
                TbBitacoraStatus nextStatus = bitacoraService.getNextStatus(bitacora.getIdStatus().getNumStatus(),bitacora.getIdEnvio().getIdEnvio(), bitacora.getIsFinal());
                Integer currentKm = bitacoraHistorico.getCountKmHistoricoByBitacora(idbitacora);
                map.put("num_status", nextStatus.getNumStatus());
                map.put("des_status", nextStatus.getDesStatus());
                map.put("id_status", nextStatus.getId());
                map.put("current_km", currentKm);
                return map;
            }
        }
        return new HashMap<String,Object>();
    }         
    
    
    @ResponseBody
    @RequestMapping(value = "/update_status", method = RequestMethod.GET, produces = "application/json")
    public TbBitacora updateStatusBitacora(@RequestParam("idbitacora") int idBitacora, @RequestParam("km") int kmActual, @RequestParam("comentario") String observaciones) {
        TbBitacora bitacora = bitacoraService.updateBitacoraStatus(idBitacora, kmActual, observaciones);
        TbUsuarios operador = bitacora.getIdOperador();
        String bodyemail = " Se actualizó la siguiente bitácora: <br>"
                + " <br> Id Bitácora: " + bitacora.getId() + "<br>"
                + " <br> Fecha recepción: " + dt1.format(bitacora.getFecha()) + " <br> "
                + " <br> Punto Origen: " + bitacora.getPtoOrigen().getNombrePunto() + "<br>"
                + " <br> Dirección Punto Origen: " + bitacora.getPtoOrigen().getDireccion() + "<br>"
                + " <br> Punto Destino: " + bitacora.getPtoDestino().getNombrePunto() + "<br>"
                + " <br> Dirección Punto Destino: " + bitacora.getPtoDestino().getDireccion() + "<br>"
                + " <br> Operador: " + (operador.getNombre() != null ? operador.getNombre() : "") + " " + (operador.getApellidoPat() != null ? operador.getApellidoPat() : "") + " " + (operador.getApellidoMat() != null ? operador.getApellidoMat() : "") + "<br>"
                + " <br> Estatus: "+bitacora.getIdStatus().getDesStatus()+"<br>";                
        TbDomainConfig config = configRepository.findOne("gnk.mails");
        mailer.sendTemplateMail(bitacora.getIdOperador().getEmail(), "Actualización de estatus: "+bitacora.getNombre(), bodyemail, config.getConfigValue(), "");        
        return bitacora;
    }    

    @ResponseBody
    @RequestMapping(value = "/historico_bitacora", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String, Object>> updateStatusBitacora(@RequestParam("idbitacora") Integer idBitacora) {
        if(idBitacora!=null){
            return bitacoraDao.getAllBitacoraHistoricoByIdBitacora(idBitacora);
        }
        return new ArrayList<Map<String, Object>>();
    }    
    
    @ResponseBody
    @RequestMapping(value = "/addhistoricoimage", method = RequestMethod.POST, produces = "application/json")
    public String addImageHistorico(@RequestParam("idBitacora") Integer idBitacora, @RequestParam("status") Integer status,@RequestParam("file") MultipartFile file){
        try{
            System.out.println("file");
            if(file!=null){
                System.out.println(file.getName());
                System.out.println(file.getContentType());
                System.out.println(file.getSize());
            }
            TbBitacoraHistorico response = bitacoraService.saveHistoricoImagen(idBitacora, status, file);
            //ObjectBeanVO histVO = new ObjectBeanVO();
            //histVO.setData(response);
            return "SUCCESS";
        }catch(IOException ex){
            logger.error(ex);
            //ObjectBeanVO histVO = new ObjectBeanVO();
            //histVO.setActionMessage(ex.getLocalizedMessage());
            return "ERROR";            
        }
    }
    
    @ResponseBody
    @RequestMapping(value = "/addbitacoraimage", method = RequestMethod.POST, produces = "application/json")
    public String addImageBitacora(@RequestParam("idbitacora") int idbitacora, @RequestParam("file") MultipartFile file){
        try{
            TbBitacora response = bitacoraService.saveBitacoraImagen(idbitacora, file);
            return "OK";
        }catch(IOException ex){
            logger.error(ex);
            return "ERROR";
        }
    }  
    
    @ResponseBody
    @RequestMapping(value = "/interrumpirRuta", method = RequestMethod.GET, produces = "application/json")
    public TbRutaInterrupcion addIInterrupcionToBitacora(@RequestParam("idusuario") Integer idUsuario, @RequestParam("idruta") Integer idRuta, @RequestParam("comentario") String comentario){
         TbRutaInterrupcion result = rutaService.setRutaInterrupcion(idRuta,comentario);
         TbRuta ruta = rutaService.getRutaById(idRuta);
         //SecurityUserBean user = (SecurityUserBean) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
         TbUsuarios user = usuarioRepository.findOne(idUsuario);
         String bodyemail = "";
         TbDomainConfig config = configRepository.findOne("gnk.mails");
         //interrupcion.setStatus(0);//0 es en proceso
         //interrupcion.setStatus(1);// el proceso de interrupcion ya fue terminado        
         if(result!=null && ruta!=null){
             switch(result.getStatus()){
                 case 0:
                    bodyemail = "La ruta con nombre: "+ruta.getNombre()+"<br>"+
                                " Se ha pausado, la hora y fecha de pausa es:"+dt1.format(result.getFechaInicio());
                     mailer.sendTemplateMail(user.getEmail(), "Interrupción de ruta: "+ruta.getNombre(), bodyemail ,config.getConfigValue(),"");
                     break;
                 case 1:
                    bodyemail = "La ruta con nombre: "+ruta.getNombre()+"<br>"+
                                " Se ha retirado la pausa, la hora y fecha para continuar es:  "+dt1.format(result.getFechaFin());
                     mailer.sendTemplateMail(user.getEmail(), "Interrupción de ruta: "+ruta.getNombre(), bodyemail ,config.getConfigValue(),"");
                     break;
             }
         }
         return result;
    }    
}
