/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author jmejia
 */
public class SecurityUserBean extends org.springframework.security.core.userdetails.User{
    private Integer idUsu;
    private String usuario;
    private String tipo;
    
    @JsonIgnore
    private String email;
    
    public SecurityUserBean(String username, String password, Collection<? extends GrantedAuthority> authorities, Integer idusu) {
        super(username, password, authorities);
        this.idUsu=idusu;
    }

    public SecurityUserBean(String username, String password, Collection<? extends GrantedAuthority> authorities, Integer idusu, String email) {
        super(username, password, authorities);
        this.idUsu=idusu;
        this.email=email;
    }

    public SecurityUserBean(String username, String password, Collection<? extends GrantedAuthority> authorities, Integer idusu, String email, String usuario, String tipo) {
        super(username, password, authorities);
        this.idUsu=idusu;
        this.email=email;
        this.usuario=usuario;
        this.tipo=tipo;
    }
    
    /**
     * @return the idUsu
     */
    public Integer getIdUsu() {
        return idUsu;
    }

    /**
     * @param idUsu the idUsu to set
     */
    public void setIdUsu(Integer idUsu) {
        this.idUsu = idUsu;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
}
