package mx.gnkl.tracking.api.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "mx.gnkl.tracking.api")
public class HelloWorldConfiguration {
	
}