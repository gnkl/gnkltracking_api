/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.configuration;

import java.util.Properties;
import javax.sql.DataSource;
import org.apache.commons.configuration.ConfigurationConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.apache.commons.configuration.DatabaseConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

/**
 *
 * @author jmejia
 */
@Configuration
@PropertySource(value = { "classpath:application.properties" })
public class GnkPropertiesConfigurer implements EnvironmentAware{

//    @Autowired 
//    private static DataSource dataSource;
    @Autowired
    private Environment env;    
    
    @Override
    public void setEnvironment(final Environment environment) {
        this.env = environment;
    }    
    
    @Bean
    public PropertyPlaceholderConfigurer placeHolderConfigurer() {
        PropertyPlaceholderConfigurer propertiesConfig = new PropertyPlaceholderConfigurer();
        propertiesConfig.setSystemPropertiesMode(PropertyPlaceholderConfigurer.SYSTEM_PROPERTIES_MODE_OVERRIDE);
   
        DatabaseConfiguration dbConfiguration = new DatabaseConfiguration(propertiesDatasource(),"tb_domain_config","config_name","config_value");
        System.out.println(dbConfiguration);
        propertiesConfig.setProperties(ConfigurationConverter.getProperties(dbConfiguration));
        return propertiesConfig;
    }
        
    @Bean
    public AnnotationMethodHandlerAdapter annotationMethodHandlerAdapter() {
        final AnnotationMethodHandlerAdapter annotationMethodHandlerAdapter = new AnnotationMethodHandlerAdapter();
        final MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
        final Jaxb2RootElementHttpMessageConverter jaxb2root = new Jaxb2RootElementHttpMessageConverter();

        HttpMessageConverter<?>[] httpMessageConverter = { jacksonConverter, jaxb2root};

        String[] supportedHttpMethods = { "POST", "GET", "HEAD" };

        annotationMethodHandlerAdapter.setMessageConverters(httpMessageConverter);
        annotationMethodHandlerAdapter.setSupportedMethods(supportedHttpMethods);

        return annotationMethodHandlerAdapter;
    }    
    
    
    @Bean
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(20971520);
        return multipartResolver;
    }    

    @Bean
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);
        mailSender.setUsername("ricardo.wence@gnkl.mx");
        mailSender.setPassword("ricardo.wence+111");
        //mailSender.setJavaMailProperties(javaMailProperties);
        Properties props = new Properties();
        props.setProperty("mail.transport.protocol", "smtp");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.starttls.enable","true");
        props.setProperty("mail.debug", "true");
        mailSender.setJavaMailProperties(props);
        return mailSender;
    }    
    
    
/*    public static DataSource propertiesDatasource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://189.194.249.164:3306/tracking_wallmart");
        //dataSource.setUrl("jdbc:mysql://192.168.0.50:3306/tracking_wallmart");
        dataSource.setUsername("root");
        dataSource.setPassword("eve9397");
        return dataSource;
    }*/
    
    public DataSource propertiesDatasource(){
        System.out.println(env.getProperty("jdbc.driverClassName"));
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        dataSource.setUrl(env.getProperty("jdbc.url"));
        dataSource.setUsername(env.getProperty("jdbc.username"));
        dataSource.setPassword(env.getProperty("jdbc.password"));
        return dataSource;
    }    
    
    @Bean
    public FreeMarkerConfigurationFactoryBean freemarkerMailConfiguration(){
        FreeMarkerConfigurationFactoryBean freeMarkerConfig = new FreeMarkerConfigurationFactoryBean();
        freeMarkerConfig.setTemplateLoaderPath("/WEB-INF/tpl");
        return freeMarkerConfig;
    }
    
}
