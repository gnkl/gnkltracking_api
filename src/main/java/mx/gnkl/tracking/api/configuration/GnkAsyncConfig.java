/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.configuration;

import java.util.concurrent.Executor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 *
 * @author jmejia
 */
@Configuration
@EnableAsync
public class GnkAsyncConfig {    
    
    @Bean(name = "genericExecutor")
    public Executor genericExecutor() {
        return new ThreadPoolTaskExecutor();
    }    
    
    @Bean(name = "myExecutor")
    public Executor myTaskExecutor() {
        return new ThreadPoolTaskExecutor();
    }        
}
