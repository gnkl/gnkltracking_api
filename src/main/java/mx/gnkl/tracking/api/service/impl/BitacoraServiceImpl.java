/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.service.impl;

import mx.gnkl.tracking.api.bean.BeanBitacora;
import mx.gnkl.tracking.api.bean.ObjectListVO;
import mx.gnkl.tracking.api.bean.SecurityUserBean;
import mx.gnkl.tracking.api.model.TbArchivos;
import mx.gnkl.tracking.api.model.TbBitacora;
import mx.gnkl.tracking.api.model.TbBitacoraHistorico;
import mx.gnkl.tracking.api.model.TbBitacoraPaquete;
import mx.gnkl.tracking.api.model.TbBitacoraStatus;
import mx.gnkl.tracking.api.model.TbHistpaq;
import mx.gnkl.tracking.api.model.TbPaquetes;
import mx.gnkl.tracking.api.model.TbPuntoEo;
import mx.gnkl.tracking.api.model.TbUsuarios;
import mx.gnkl.tracking.api.model.TbVehiculo;
import mx.gnkl.tracking.api.persistence.dao.BitacoraDao;
import mx.gnkl.tracking.api.persistence.jpa.ArchivoRepository;
import mx.gnkl.tracking.api.persistence.jpa.BitacoraHistoricoRepository;
import mx.gnkl.tracking.api.persistence.jpa.BitacoraPaqueteRepository;
import mx.gnkl.tracking.api.persistence.jpa.BitacoraRepository;
import mx.gnkl.tracking.api.persistence.jpa.BitacoraStatusRepository;
import mx.gnkl.tracking.api.persistence.jpa.HistoricoRepository;
import mx.gnkl.tracking.api.persistence.jpa.PuntoRepository;
import mx.gnkl.tracking.api.persistence.jpa.UserRepository;
import mx.gnkl.tracking.api.service.BitacoraService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
@Service
public class BitacoraServiceImpl implements BitacoraService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BitacoraStatusRepository bitacoraStatusRepository;
    
    @Autowired
    private BitacoraRepository bitacoraRepository;

    @Autowired
    private BitacoraHistoricoRepository historicoRepository;

    @Autowired
    private HistoricoRepository paqHistoricoRepository;
    
    @Autowired
    private PuntoRepository puntoRepository;
    
    @Autowired
    private BitacoraPaqueteRepository bitacoraPaqueteRepository;
    
    @Autowired
    private BitacoraDao bitacoraDao;
    
    @Autowired
    private BitacoraHistoricoRepository bitacoraHistorico;

    @Autowired
    private ArchivoRepository archivoRepository;
    
//    @Autowired    
//    private BitacoraInterrupcionRepository   interrupcionRepository;
    
    @Value("${bitacora.origen}")
    private Integer bitacoraOrigen;    

    @Value("${bitacora.destino}")
    private Integer bitacoraDestino;
    
    @Value("${bitacora.imagenes}")
    private String pathBitacora = "/archivo/tracking/bitacora/";

    @Value("${historico.imagenes}")
    private String pathHistorico;
    
    
    @Override
    @Transactional
    public TbBitacora createBitacora(TbBitacora bitacora) {
        if(bitacora.getId()!=null){
            return bitacora;
        }
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        TbUsuarios usuario = userRepository.findOne(user.getIdUsu());
        if(bitacora.getIdEnvio()!=null){
            //TbBitacoraStatus status = bitacoraStatusRepository.getStatusByEmpresaNoEstatus(2,1);
            TbBitacoraStatus status = bitacoraStatusRepository.getStatusByTipoEnvioNoEstatus(bitacora.getIdEnvio().getIdEnvio(),1);
            bitacora.setIdStatus(status);
            bitacora.setIdUsuario(usuario);
            bitacora.setHoraSalida(null);
            bitacora.setHoraLlegada(null);
            bitacora.setKmFinal(0);
            bitacora.setKmInicial(0);
            bitacora.setFechaCreacion(new Date());
            bitacora.setNombre(bitacora.getPtoOrigen().getNombrePunto()+" - "+bitacora.getPtoDestino().getNombrePunto());
            bitacora.setIsPausa(0);
            bitacora.setActivo(1);
            bitacora = bitacoraRepository.save(bitacora);

            TbBitacoraHistorico bitacoraHistorico = new TbBitacoraHistorico();

            bitacoraHistorico.setEstatus(status);
            bitacoraHistorico.setFecha(new Date());
            bitacoraHistorico.setIdBitacora(bitacora);
            bitacoraHistorico.setKilometraje(0);

            bitacoraHistorico = historicoRepository.save(bitacoraHistorico);
            
        }
        
        return bitacora;
    }

    @Override
    public TbBitacora updateBitacora(TbBitacora bitacora) {
        
        TbBitacora bitacoraFound = bitacoraRepository.findOne(bitacora.getId());
        
        bitacoraFound.setIdOperador(bitacora.getIdOperador());
        bitacoraFound.setIdVehiculo(bitacora.getIdVehiculo());
        bitacoraFound.setPtoOrigen(bitacora.getPtoOrigen());
        bitacoraFound.setPtoDestino(bitacora.getPtoDestino());
        bitacoraFound.setFecha(bitacora.getFecha());
        bitacoraFound.setIsFinal(bitacora.getIsFinal());
        
        bitacoraFound = bitacoraRepository.save(bitacoraFound);
        
//        if(bitacora.getIsFinal()==1){
//            setOthersBitacorasNotFinal(bitacora.getIdRuta().getId(), bitacora.getId());
//        }
//        
//        setLastBitacoraFinalIfEmptyList(bitacora.getIdRuta().getId());
        
        return bitacoraFound;
    }
    
    public void setOthersBitacorasNotFinal(Integer idRuta, Integer idBitacora){
        List<TbBitacora> listBitacoraFinal = bitacoraRepository.getListBitacoraFinalByRuta(idRuta);
        if(listBitacoraFinal!=null && !listBitacoraFinal.isEmpty()){
            for(TbBitacora bitacora : listBitacoraFinal){
                if(bitacora.getId()!=null && idBitacora!=null && bitacora.getId().intValue()!=idBitacora.intValue()){
                    bitacora.setIsFinal(0);
                    bitacoraRepository.save(bitacora);
                }
            }
        }
    }
    
    public void setLastBitacoraFinalIfEmptyList(Integer idRuta){
        List<TbBitacora> listBitacoraFinal = bitacoraRepository.getListBitacoraFinalByRuta(idRuta);
        if(listBitacoraFinal!=null && listBitacoraFinal.isEmpty()){
            Integer idBitacora = bitacoraRepository.getMaxIdBitacoraByRuta(idRuta);
            TbBitacora lastBitacora = bitacoraRepository.findOne(idBitacora);
            if(lastBitacora!=null){
                lastBitacora.setIsFinal(1);
                bitacoraRepository.save(lastBitacora);
            }
        }
    }    

    @Override
    public TbBitacora getBitacoraById(Integer idBitacora) {
        return bitacoraRepository.findOne(idBitacora);
    }

    private void putOthersBitacoraInactiveByRutaBitacora(Integer idRuta, Integer idBitacora){
        bitacoraDao.updateBitacoraRutaActivoInactivo(idRuta, idBitacora, 0);
    }

    private void putBitacoraActiveByRuta(Integer idRuta){
        bitacoraDao.updateBitacoraRutaActivoInactivo(idRuta, 1);
    }
    
    @Override
    @Transactional
    public TbBitacora updateBitacoraStatus(Integer idBitacora, Integer kilometraje, String observaciones) {
        TbBitacora bitacora = bitacoraRepository.findOne(idBitacora);
        TbBitacoraStatus status = bitacora.getIdStatus();
        TbBitacoraStatus newStatus=null;
        Integer kmInicial=0;
        Integer kmFinal=0;
        Integer empresa=0;
        if(kilometraje==0 || kilometraje<0 || kilometraje==null){
            kilometraje = bitacoraDao.getMaxKilometrajeByIdBitacora(idBitacora);
        }
        //TODO 6 estatus
        //empresa = 0
        
        switch(status.getNumStatus()){
            case 1:
                newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa,status.getNumStatus()+1,bitacora.getIdEnvio().getIdEnvio());
                bitacora.setIdStatus(newStatus);
                kmInicial = kilometraje;
                bitacora.setFecha(new Date());
                bitacora.setKmInicial(kmInicial);
                bitacora.setHoraSalida(new Date());
                bitacora.setHoraLlegada(null);
                bitacora.setFechaFin(null);
                putOthersBitacoraInactiveByRutaBitacora(bitacora.getIdRuta().getId(), bitacora.getId());
                break;
            case 2:
            case 3:    
                newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa,status.getNumStatus()+1, bitacora.getIdEnvio().getIdEnvio());
                bitacora.setIdStatus(newStatus);
                //kmFinal = kilometraje;
                bitacora.setKmFinal(0);
                bitacora.setHoraLlegada(null);
                bitacora.setFechaFin(null);
                break;
            case 4:
                newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa,status.getNumStatus()+1, bitacora.getIdEnvio().getIdEnvio());
                if(bitacora.getIsFinal()==0){
                    bitacora.setIdStatus(newStatus);
                    //kmFinal = kilometraje;
                    bitacora.setKmFinal(kilometraje);
                    bitacora.setHoraLlegada(new Date());
                    bitacora.setFechaFin(new Date());
                    putBitacoraActiveByRuta(bitacora.getIdRuta().getId());
                } else {
                    bitacora.setIdStatus(newStatus);
                    //kmFinal = kilometraje;
                    bitacora.setKmFinal(0);
                    bitacora.setHoraLlegada(null); 
                    bitacora.setFechaFin(null);
                }
                break;                
            case 5:
                if(bitacora.getIsFinal()==0){
                    newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa,status.getNumStatus(), bitacora.getIdEnvio().getIdEnvio());
                }else{
                    newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa,status.getNumStatus()+1, bitacora.getIdEnvio().getIdEnvio());
                    bitacora.setIdStatus(newStatus);
                    //kmFinal = kilometraje;
                    bitacora.setKmFinal(0);
                    bitacora.setHoraLlegada(null);
                    bitacora.setFechaFin(null);
                }
                break;
            case 6: 
                newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa,status.getNumStatus()+1, bitacora.getIdEnvio().getIdEnvio());
                bitacora.setIdStatus(newStatus);
                //kmFinal = kilometraje;
                bitacora.setKmFinal(kilometraje);
                bitacora.setHoraLlegada(new Date());
                bitacora.setFechaFin(new Date());
                putBitacoraActiveByRuta(bitacora.getIdRuta().getId());
                break;
            case 7:    
            //case 8:
                newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa,status.getNumStatus(), bitacora.getIdEnvio().getIdEnvio());
                bitacora.setIdStatus(newStatus);
                //kmFinal = kilometraje;
                bitacora.setKmFinal(kilometraje);
                bitacora.setHoraLlegada(new Date()); 
                bitacora.setFechaFin(new Date());
                //putBitacoraActiveByRuta(bitacora.getIdRuta().getId());
                break;
        }
        
        if(status.getNumStatus()< 5 && bitacora.getIsFinal()==0){
            bitacora = bitacoraRepository.save(bitacora);

            TbBitacoraHistorico bitacoraHistorico = new TbBitacoraHistorico();

            bitacoraHistorico.setEstatus(newStatus);
            bitacoraHistorico.setFecha(new Date());
            bitacoraHistorico.setIdBitacora(bitacora);
            bitacoraHistorico.setKilometraje(kilometraje);
            bitacoraHistorico.setObservaciones(observaciones);

            bitacoraHistorico = historicoRepository.save(bitacoraHistorico);            
        }else if (status.getNumStatus()< 7 && bitacora.getIsFinal()==1){
            bitacora = bitacoraRepository.save(bitacora);

            TbBitacoraHistorico bitacoraHistorico = new TbBitacoraHistorico();

            bitacoraHistorico.setEstatus(newStatus);
            bitacoraHistorico.setFecha(new Date());
            bitacoraHistorico.setIdBitacora(bitacora);
            bitacoraHistorico.setKilometraje(kilometraje);
            bitacoraHistorico.setObservaciones(observaciones);

            bitacoraHistorico = historicoRepository.save(bitacoraHistorico);                        
        }
        
        return bitacora;
    }


    @Override
    @Transactional
    public TbBitacora cancelBitacoraStatus(Integer idBitacora) {
        TbBitacora bitacora = bitacoraRepository.findOne(idBitacora);
        TbBitacoraStatus newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatus(0,8);
        bitacora.setIdStatus(newStatus);
        bitacora.setKmInicial(0);
        bitacora.setKmFinal(0);
        bitacora.setHoraSalida(null);
        bitacora.setHoraLlegada(null);
        
        bitacora = bitacoraRepository.save(bitacora);

        TbBitacoraHistorico bitacoraHistorico = new TbBitacoraHistorico();

        bitacoraHistorico.setEstatus(newStatus);
        bitacoraHistorico.setFecha(new Date());
        bitacoraHistorico.setIdBitacora(bitacora);
        bitacoraHistorico.setKilometraje(0);

        bitacoraHistorico = historicoRepository.save(bitacoraHistorico);         
        
        return bitacora;
    }
    
    
    @Override
    public TbBitacoraStatus getNextStatus(Integer status, Integer tipoEnvio, Integer isFinal) {
        TbBitacoraStatus bitacoraStatus=null;
        Integer empresa=0;
        if(isFinal==0){
            switch(status){
                case 1:
                case 2:
                case 3:
                case 4:    
                    bitacoraStatus=bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa, status+1, tipoEnvio);
                    break;
                case 5:
                case 8:    
                default:    
                    bitacoraStatus=bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa, status, tipoEnvio);
                    break;
            }            
        } else if (isFinal==1) {
            switch(status){
                case 1:
                case 2:
                case 3:
                case 4:    
                case 5:    
                case 6:    
                    bitacoraStatus=bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa, status+1, tipoEnvio);
                    break;
                case 7:
                case 8:    
                default:    
                    bitacoraStatus=bitacoraStatusRepository.getStatusByEmpresaNoEstatusEnvio(empresa, status, tipoEnvio);
                    break;
            }            
        }

        return bitacoraStatus;
    }

    @Override
    public List<TbBitacoraHistorico> getListHistoricoBitacoraByIdBitacora(Integer idBitacora) {
        return historicoRepository.getAllHistoricoByBitacora(idBitacora);
    }
    
    @Override
    public List<Map<String,Object>> getDefaultPuntoEOBitacoras() {
        List<Map<String,Object>> result1 = new ArrayList<Map<String,Object>>();
        Map<String,Object> map = new HashMap<String,Object>();
        map.put("origen", puntoRepository.findOne(bitacoraOrigen));
        map.put("destino", puntoRepository.findOne(bitacoraDestino));                                                        
        result1.add(map);
        return result1;
    }      
    
    @Override
    public TbBitacora getBitacoraByDate(Integer idOperador, Integer tipoEnvio) {
        Date today = new Date();
        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
        String todayString = dt1.format(today);
        TbBitacora bitacora = null;
        if(idOperador!=null && idOperador>0){
            bitacora = bitacoraRepository.getBitacoraByDate(todayString, idOperador, tipoEnvio);
        }else{
            bitacora = bitacoraRepository.getBitacoraByDate(todayString);
        }
        return bitacora;
    }       

    @Override
    @Transactional
    public TbBitacora finishBitacoraByPaquete(TbPaquetes paquete, Integer kilometraje) {
        TbBitacora currentBitacora = null;
        List<TbBitacoraPaquete> result = bitacoraPaqueteRepository.getAllBitacoraPaqueteByIdPaquete(paquete.getIdPaq());
        if(!result.isEmpty()){
            TbBitacoraPaquete item = result.get(0);
            currentBitacora = item.getIdBitacora();
            //if(currentBitacora!=null && currentBitacora.getId()!=null && (currentBitacora.getIdStatus().getNumStatus()!=5 && currentBitacora.getIdStatus().getNumStatus()!=4)){
            if(currentBitacora!=null && currentBitacora.getId()!=null){
                TbBitacoraStatus newStatus = bitacoraStatusRepository.getStatusByEmpresaNoEstatus(2,5);
                //TODO debemos revisar que va a psar con la serie de estatus , saber si se tiene en el listado de clientes o no
                //TODO verificar si tenemos que enviar o no correo
                currentBitacora.setIdStatus(newStatus);
                currentBitacora.setKmFinal(kilometraje);
                currentBitacora.setHoraLlegada(new Date());

                bitacoraRepository.save(currentBitacora);
                
                TbBitacoraHistorico bitacoraHistorico = new TbBitacoraHistorico();
                
                bitacoraHistorico.setEstatus(newStatus);
                bitacoraHistorico.setFecha(new Date());
                bitacoraHistorico.setIdBitacora(currentBitacora);
                bitacoraHistorico.setKilometraje(kilometraje);

                bitacoraHistorico = historicoRepository.save(bitacoraHistorico);                     
            }
        }
        return currentBitacora;
    }
    
    
    @Override
    @Transactional
    public TbBitacoraPaquete associatePaqueteBitacora(TbPaquetes paquete, Integer idBitacora) {
        TbBitacora currentBitacora = bitacoraRepository.findOne(idBitacora);
        TbBitacoraPaquete bitacoraPaquete = new TbBitacoraPaquete();
        bitacoraPaquete.setIdBitacora(currentBitacora);
        bitacoraPaquete.setIdPaquete(paquete);
        bitacoraPaquete.setNoTracking(paquete.getNoEmbarque());
        bitacoraPaquete = bitacoraPaqueteRepository.save(bitacoraPaquete);
        return bitacoraPaquete;
    }    

//    @Override
//    public List<String> getListBitacorasById(String idBitacora) {
//        idBitacora = "%"+idBitacora+"%";
//        return bitacoraDao.getListByIdBitacora(idBitacora);
//    }

    @Override
    public ObjectListVO searchByParameters(String fechaInicial, String fechaFinal, String idBitacora, String noTracking, String idEnvio,Integer page, Integer maxResults) {
        List<TbBitacora> list = bitacoraDao.searchByParametersInfo(fechaInicial, fechaFinal, noTracking, idBitacora,idEnvio, page, maxResults);
        Long size = bitacoraDao.searchByParametersInfo(idBitacora, idEnvio);
        return new ObjectListVO(page, size, list);   
    }
    
    @Override
    public List<BeanBitacora> getListToPutIntoBitacoraExcel(String idBitacora){
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat hourformat = new SimpleDateFormat("HH:mm:ss");
        List<BeanBitacora> listHeader = new ArrayList<BeanBitacora>();
        List<BeanBitacora> listData = new ArrayList<BeanBitacora>();
        List<BeanBitacora> listResult = new ArrayList<BeanBitacora>();
        TbHistpaq historico = null;
        
        TbBitacora bitacora = bitacoraRepository.findOne(Integer.parseInt(idBitacora));
        
        TbBitacoraHistorico llegadaCedisCliente = bitacoraHistorico.getHistoricoByEstatusBitacora(8,bitacora.getId());
        
        TbBitacoraHistorico salidaCedisCliente = bitacoraHistorico.getHistoricoByEstatusBitacora(10,bitacora.getId());
        
        if(bitacora!=null && llegadaCedisCliente!=null){            
                        
            TbUsuarios operador = bitacora.getIdOperador();
            TbVehiculo vehiculo = bitacora.getIdVehiculo();
            TbPuntoEo ptoEntrega = bitacora.getPtoDestino();
            TbPuntoEo ptoOrigen = bitacora.getPtoOrigen();
            String operadorString = operador.getNombre()+" "+operador.getApellidoPat()+" "+operador.getApellidoMat();
            String fecha = dateformat.format(bitacora.getFecha());
//            String vehiculoString = vehiculo.getNoEconomico()+" "+vehiculo.getDescripcion();
            String vehiculoString = vehiculo.getNoEconomico();
            String rutaString = ptoOrigen.getNombrePunto()+" - "+ptoEntrega.getNombrePunto();
            String kmInicial = bitacora.getKmInicial().toString();
            String kmFinal = bitacora.getKmFinal().toString();
            String horaSalida = hourformat.format(bitacora.getHoraSalida());
            String horaLlegada = hourformat.format(bitacora.getHoraLlegada());

            //listData.add(new BeanBitacora(0,0,"Recolección"));
            
            listHeader.add(new BeanBitacora(0,2,ptoOrigen.getNombrePunto()));

            listHeader.add(new BeanBitacora(0,8,ptoEntrega.getNombrePunto()));            

            listHeader.add(new BeanBitacora(1,14,fecha));

            listHeader.add(new BeanBitacora(3,13,operadorString));

            listHeader.add(new BeanBitacora(5,14,vehiculoString));

            listHeader.add(new BeanBitacora(6,14,"JALOMA"));

            listHeader.add(new BeanBitacora(9,13,kmInicial));

            listHeader.add(new BeanBitacora(11,13,kmFinal));
            
            listHeader.add(new BeanBitacora(13,13,horaSalida));

            listHeader.add(new BeanBitacora(15,13,horaLlegada));
            

            
            
//            
//            if(bitacoraPaqueteList!=null && !bitacoraPaqueteList.isEmpty()){
//                for(TbBitacoraPaquete item : bitacoraPaqueteList){
//                    List<TbHistpaq> list = paqHistoricoRepository.getAllPaquetesByIdPaqStatus(item.getIdPaquete().getIdPaq(), 36);
//                    if(list!=null && !list.isEmpty()){
//                        historico = list.get(0);
//                        break;
//                    }
//                }                
//            }
//            
//            int j=0;
//            
//            for(int i=0;i<bitacoraPaqueteList.size();i++){
////                if(i%4==0){
////                    listData.addAll(listHeader);
////                    listResult.add(listData);
////                    j=0;
////                } else {
//                    List<BeanBitacora> listDetalle = getListDetalle(i, bitacoraPaqueteList.get(i), llegadaCedisCliente, historico);
//                    if(listDetalle!=null && !listDetalle.isEmpty()){
//                        listData.addAll(listDetalle);
//                    }
////                    j++;
////                }
//            }
//            listData.addAll(listHeader);
            List<BeanBitacora> listDetalle = getListDetalle(0, llegadaCedisCliente, salidaCedisCliente, bitacora);
            listResult.addAll(listHeader);
            listResult.addAll(listDetalle);
        }        
        return listResult;
    }
    
    public List<BeanBitacora> getListDetalle(Integer numDetalle, TbBitacoraHistorico llegadaCedisCliente, TbBitacoraHistorico salidaCedisCliente, TbBitacora bitacora){
        List<BeanBitacora> listDetalle = new ArrayList<BeanBitacora>();
        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat hourformat = new SimpleDateFormat("HH:mm:ss");
        
        String fechaVisita = dateformat.format(llegadaCedisCliente.getFecha());
        String hourLlegada = hourformat.format(llegadaCedisCliente.getFecha());
        String hourSalida = "";
        String kilometraje = llegadaCedisCliente.getKilometraje().toString();

        int rowCount=0;
        int cellCount=0;
        int rowTracking=0;
        int cellTracking=0;
        int trackingCount=0;
        //item 0 11,3 12,3 13,3 14,3
        //item 1 11,9 12,9 13,9 14,9
        //item 2 27,3 28,3 29,3 30,3
        //item 3 27,9 28,9 29,9 30,9
        
        //para tracking
        //item 0 2,4 
        //item 1 2,10
        //item 2 18,4
        //item 3 18,10
        
        if(numDetalle==0 || numDetalle==1){
            rowCount=11;
            rowTracking=2;
        }else if(numDetalle==2 || numDetalle==3){
            rowCount=27;
            rowTracking=18;
        }
        
        if (numDetalle % 2 == 0) {
            cellCount=3;
            cellTracking=4;
        } else {
            cellCount=9;
            cellTracking=10;
        }        
        
        listDetalle.add(new BeanBitacora(rowCount,cellCount,fechaVisita));
        listDetalle.add(new BeanBitacora(++rowCount,cellCount,hourLlegada));

        if(llegadaCedisCliente!=null){
            hourSalida = hourformat.format(llegadaCedisCliente.getFecha());
            listDetalle.add(new BeanBitacora(++rowCount,cellCount,hourSalida));            
        }
        
        listDetalle.add(new BeanBitacora(++rowCount,cellCount,kilometraje));

        //String tracking = bitacora.getNombre().s
        String[] arrayTracking = bitacora.getNombre().split(" ");
        
        listDetalle.add(new BeanBitacora(rowTracking,cellTracking,arrayTracking[0]));
        

        return listDetalle;
    }
    
    
    
//    public List<BeanBitacora> getListDetalle(Integer numDetalle, TbBitacoraPaquete bitacoraPaquete, TbBitacoraHistorico llegadaCedisCliente, TbHistpaq response){
//        List<BeanBitacora> listDetalle = new ArrayList<BeanBitacora>();
//        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
//        SimpleDateFormat hourformat = new SimpleDateFormat("HH:mm:ss");
//        
//        String fechaVisita = dateformat.format(llegadaCedisCliente.getFecha());
//        String hourLlegada = hourformat.format(llegadaCedisCliente.getFecha());
//        String hourSalida = "";
//        String kilometraje = llegadaCedisCliente.getKilometraje().toString();
//
//        int rowCount=0;
//        int cellCount=0;
//        int rowTracking=0;
//        int cellTracking=0;
//        int trackingCount=0;
//        //item 0 11,3 12,3 13,3 14,3
//        //item 1 11,9 12,9 13,9 14,9
//        //item 2 27,3 28,3 29,3 30,3
//        //item 3 27,9 28,9 29,9 30,9
//        
//        //para tracking
//        //item 0 2,4 
//        //item 1 2,10
//        //item 2 18,4
//        //item 3 18,10
//        
//        if(numDetalle==0 || numDetalle==1){
//            rowCount=11;
//            rowTracking=2;
//        }else if(numDetalle==2 || numDetalle==3){
//            rowCount=27;
//            rowTracking=18;
//        }
//        
//        if (numDetalle % 2 == 0) {
//            cellCount=3;
//            cellTracking=4;
//        } else {
//            cellCount=9;
//            cellTracking=10;
//        }        
//        
//        listDetalle.add(new BeanBitacora(rowCount,cellCount,fechaVisita));
//        listDetalle.add(new BeanBitacora(++rowCount,cellCount,hourLlegada));
//
//        if(response!=null){
//            hourSalida = hourformat.format(response.getFechaHoraHist());
//            listDetalle.add(new BeanBitacora(++rowCount,cellCount,hourSalida));            
//        }
//        
//        listDetalle.add(new BeanBitacora(++rowCount,cellCount,kilometraje));
//
//        String tracking=bitacoraPaquete.getNoTracking();
//        
//        if(tracking.indexOf("-")>0 || tracking.indexOf(" ")>0){
//            if(tracking.indexOf("-")>0){
//                String[] trackingSeparate = tracking.split("-");
//                for(String name: trackingSeparate){
//                    listDetalle.add(new BeanBitacora(rowTracking++,cellTracking,name));
//                }
//            }
//            if(tracking.indexOf(" ")>0){
//                String[] trackingSeparate = tracking.split(" ");
//                for(String name: trackingSeparate){
//                    listDetalle.add(new BeanBitacora(rowTracking++,cellTracking,name));
//                }            
//            }
//        }else{
//            listDetalle.add(new BeanBitacora(rowTracking,cellTracking,bitacoraPaquete.getNoTracking()));
//        }
////        listDetalle.add(new BeanBitacora(rowTracking,cellTracking,bitacoraPaquete.getNoTracking()));
//
//        return listDetalle;
//    }

    @Override //TODO add posibility to add more files 
    public TbBitacora saveBitacoraImagen(Integer idBitacora, MultipartFile file) throws IOException {
        TbBitacora bitacora = bitacoraRepository.findOne(idBitacora);
        TbArchivos archivo = new TbArchivos();
        archivo.setPathArchivo(persistFileToDirectory(file, idBitacora));
        bitacora.getArchivos().add(archivo);
        return bitacoraRepository.save(bitacora);
    }
    
    private String persistFileToDirectory(MultipartFile file, Integer id)throws IOException{
        InputStream inputStream = null;
        OutputStream outputStream = null;
                
        String fileName = file.getOriginalFilename();
        File newFile = new File(pathBitacora+ id +"/"+fileName);
        
        File newdirectory = new File(pathBitacora+ id);
        inputStream = file.getInputStream();
        if(!newdirectory.exists()){
            newdirectory.mkdir();
        }

        if (!newFile.exists()) {
            newFile.createNewFile();
        }
        outputStream = new FileOutputStream(newFile);
        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        return newFile.getPath();
    }

    private String persistFileToDirectoryPath(String path, MultipartFile file, Integer id)throws IOException{
        InputStream inputStream = null;
        OutputStream outputStream = null;
                
        String fileName = file.getOriginalFilename();
        File newFile = new File(path+ id +"/"+fileName);
        
        File newdirectory = new File(path+ id);
        inputStream = file.getInputStream();
        if(!newdirectory.exists()){
            newdirectory.mkdir();
        }

        if (!newFile.exists()) {
            newFile.createNewFile();
        }
        outputStream = new FileOutputStream(newFile);
        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        return newFile.getPath();
    }    
    
    @Override
    public byte[] getImageFromBitacora(Integer idBitacora) throws IOException {
        //TbHistpaq historico = historicoRepository.findOne(idHist);
        TbBitacora bitacora = bitacoraRepository.findOne(idBitacora);
        if(bitacora.getArchivos()!=null && !bitacora.getArchivos().isEmpty()){
            String filename = String.valueOf(idBitacora + (new Date().getTime())) + ".zip";
            File zipfile = new File(pathBitacora+filename);
            // Create a buffer for reading the files
            byte[] buf = new byte[1024];
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
            int i=0;
            Iterator<TbArchivos> archivos = bitacora.getArchivos().iterator();
            System.out.println(bitacora.getArchivos().size());
            while (archivos.hasNext()) {
                i++;
                TbArchivos archivo = archivos.next();
                //String filepath = archivo.getPathArchivo();
                Path path = Paths.get(archivo.getPathArchivo());
                File file = path.toFile();
                FileInputStream in = new FileInputStream(file);
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(i+file.getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            }
            out.close();
            byte[] data = Files.readAllBytes(zipfile.toPath());
            return data;       
        }
         return null;
    }    
    
    @Override
    public byte[] getImageFromHistorico(Integer historico) throws IOException {
        //TbHistpaq historico = historicoRepository.findOne(idHist);
        //TbBitacora bitacora = bitacoraRepository.findOne(idBitacora);
        TbBitacoraHistorico bitacoraHistoricoObj = historicoRepository.findOne(historico);
        if(bitacoraHistoricoObj!=null && bitacoraHistoricoObj.getArchivos()!=null && !bitacoraHistoricoObj.getArchivos().isEmpty()){
            String filename = String.valueOf(historico + (new Date().getTime())) + ".zip";
            File zipfile = new File(pathBitacora+filename);
            // Create a buffer for reading the files
            byte[] buf = new byte[1024];
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
            int i=0;
            Iterator<TbArchivos> archivos = bitacoraHistoricoObj.getArchivos().iterator();
            System.out.println(bitacoraHistoricoObj.getArchivos().size());
            while (archivos.hasNext()) {
                i++;
                TbArchivos archivo = archivos.next();
                //String filepath = archivo.getPathArchivo();
                Path path = Paths.get(archivo.getPathArchivo());
                File file = path.toFile();
                FileInputStream in = new FileInputStream(file);
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(i+file.getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            }
            out.close();
            byte[] data = Files.readAllBytes(zipfile.toPath());
            return data;       
        }
         return null;
    }      
    
    @Override
    public TbBitacoraPaquete getPaqueteBitacora(TbPaquetes paquete) {
        TbBitacoraPaquete bitacoraPaquete = bitacoraPaqueteRepository.getBitacoraPaqueteByIdPaquete(paquete.getIdPaq());
        return bitacoraPaquete;
    }
    
    @Override
    public void deleteArchivosByIdBitacora(Integer idBitacora)throws IOException {
        TbBitacora result = bitacoraRepository.findOne(idBitacora);
        List<TbArchivos> listarchivos = new ArrayList<TbArchivos>();
        listarchivos.addAll(result.getArchivos());
        result.setArchivos(null);
        
        bitacoraRepository.save(result);        
        
        for(TbArchivos archivo: listarchivos){
            archivoRepository.delete(archivo);
        }

        FileUtils.deleteDirectory(new File(pathBitacora+idBitacora));
        
    }    

    
/*
    @Override
    public List<String> getListBitacorasById(String idBitacora) {
        idBitacora = "%"+idBitacora+"%";
        return bitacoraDao.getListByIdBitacora(idBitacora);
    }
    
    */    
    @Override
    public List<TbBitacora> getListBitacorasById(String idBitacora) {
        //idBitacora = "%"+idBitacora+"%";
        return bitacoraRepository.getListByIdBitacora(idBitacora);
    }

    @Override
    public List<TbBitacora> getListBitacorasByName(String name) {
        name = "%"+name+"%";
        return bitacoraRepository.getListByBitacoraNombre(name);
    }

    @Override
    public List<TbBitacoraHistorico> getListHistoricoBitacoraByIdBitacora(Integer idBitacora, List<Integer> statusList) {
        return historicoRepository.getAllHistoricoByBitacora(idBitacora, statusList);
    }

    @Override
    public TbBitacoraHistorico updateHistoricoObservaciones(Integer idHistorico, String Observaciones) {
        TbBitacoraHistorico historico = bitacoraHistorico.findOne(idHistorico);
        if(historico!=null){
            historico.setObservaciones(Observaciones);
            bitacoraHistorico.save(historico);
        }
        return historico;
    }

    @Override
    public TbBitacoraHistorico saveHistoricoImagen(Integer idBitacora, Integer status, MultipartFile file) throws IOException {
        System.out.println("status: "+status); 
        System.out.println("bitacora id: "+idBitacora); 
        TbBitacoraHistorico result = historicoRepository.getHistoricoByEstatusBitacora(status,idBitacora);
        System.out.println("result: "+result); 
        TbArchivos archivo = new TbArchivos();
        archivo.setPathArchivo(persistFileToDirectoryPath(pathHistorico, file, result.getId()));
        result.getArchivos().add(archivo);
        //return bitacoraRepository.save(bitacora);
        return historicoRepository.save(result);
    }

    @Override
    public List<Map<String, Object>> searchBitacoraByParameters(String fechaInicio, String fechaFin, Integer idOperador, Integer idEnvio, Integer page, Integer maxResults) {
        return bitacoraDao.getListBitacoraByParameters(fechaInicio,fechaFin,idOperador, idEnvio, page, maxResults);
    }
    
    @Override
    public Long getCountBitacoraByParameters(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio) {
        return bitacoraDao.getCountBitacoraByParameters(fechaInicio, fechaFin, idOperador, idEnvio);
    }

//    @Override
//    public TbBitacoraInterrupcion addUpdateInterrucionToBitacora(Integer idBitacora) {
//        TbBitacora bitacora = bitacoraRepository.findOne(idBitacora);
//        TbBitacoraInterrupcion interrupcion = new TbBitacoraInterrupcion();
//        if(bitacora!=null && bitacora.getId()!=null){  
//            interrupcion = interrupcionRepository.getBitacoraInterrupcionByBitacoraEstatus(idBitacora, bitacora.getIdStatus().getId());
//            if(interrupcion!=null && interrupcion.getId()!=null){
//                interrupcion.setFechaFin(new Date());
//                interrupcion = interrupcionRepository.save(interrupcion);
//                bitacora.setIsPausa(0);
//                bitacoraRepository.save(bitacora);
//            }else{
//                interrupcion = new TbBitacoraInterrupcion();
//                interrupcion.setFechaInicio(new Date());
//                interrupcion.setIdBitacora(bitacora);
//                interrupcion.setIdEstatus(bitacora.getIdStatus());                
//                interrupcion = interrupcionRepository.save(interrupcion);
//                bitacora.setIsPausa(1);
//                bitacoraRepository.save(bitacora);
//            }
//            return interrupcion;
//        } 
//        
////        if(bitacora!=null){
////            bitacora.setIsPausa(pausa);
////            bitacoraRepository.save(bitacora);
////        }
//        
//        return new TbBitacoraInterrupcion();
//    }
}
