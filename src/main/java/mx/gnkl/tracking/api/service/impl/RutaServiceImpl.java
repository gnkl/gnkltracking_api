/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.service.impl;

import mx.gnkl.tracking.api.bean.BeanBitacoraRuta;
import mx.gnkl.tracking.api.bean.SecurityUserBean;
import mx.gnkl.tracking.api.model.TbArchivos;
import mx.gnkl.tracking.api.model.TbBitacora;
import mx.gnkl.tracking.api.model.TbBitacoraStatus;
import mx.gnkl.tracking.api.model.TbDomainConfig;
import mx.gnkl.tracking.api.model.TbPuntoEo;
import mx.gnkl.tracking.api.model.TbRuta;
import mx.gnkl.tracking.api.model.TbRutaInterrupcion;
import mx.gnkl.tracking.api.model.TbTipoenvio;
import mx.gnkl.tracking.api.model.TbUsuarios;
import mx.gnkl.tracking.api.model.TbVehiculo;
import mx.gnkl.tracking.api.persistence.dao.RutaDao;
import mx.gnkl.tracking.api.persistence.jpa.BitacoraRepository;
import mx.gnkl.tracking.api.persistence.jpa.BitacoraStatusRepository;
import mx.gnkl.tracking.api.persistence.jpa.DomainConfigRepository;
import mx.gnkl.tracking.api.persistence.jpa.EnvioRepository;
import mx.gnkl.tracking.api.persistence.jpa.PuntoRepository;
import mx.gnkl.tracking.api.persistence.jpa.RutaInterrupcionRepository;
import mx.gnkl.tracking.api.persistence.jpa.RutaRepository;
import mx.gnkl.tracking.api.persistence.jpa.UserRepository;
import mx.gnkl.tracking.api.persistence.jpa.VehiculoRepository;
import mx.gnkl.tracking.api.service.RutaService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
@Service
public class RutaServiceImpl implements RutaService{

    public final int CREATE_RUTA=1;
    public final int CREATE_PROCESS=2;
    public final int CREATE_INVALIDATE=3;
    
    @Autowired
    private RutaRepository rutaRepository;
    
    @Autowired
    private BitacoraRepository bitacoraRepository;

    @Autowired
    private EnvioRepository tipoEnvioRepository;

    @Autowired
    private PuntoRepository puntoRepository;
    
    @Autowired
    private BitacoraStatusRepository bitacoraStatusRepository;

    @Autowired
    private UserRepository usuarioRepository;

    @Autowired
    private VehiculoRepository vehiculoRepository;
    
    @Autowired
    private RutaInterrupcionRepository interrupcionRepository;
    
    @Autowired
    private RutaDao rutaDao;
    
    @Value("${punto.path}")
    private String csvfilePath;    
    
//    @Resource
//    private ProcessExcelFile fileProcesorExcel;        
//    
    @Autowired
    private DomainConfigRepository configRepository;    
//    
//    @Resource
//    private ApplicationMailer mailer;     
    
    @Value("${bitacora.imagenes}")
    private String pathBitacora = "/archivo/bitacora/reporte/";    

    @Value("${ruta.imagenes}")
    private String pathRuta;
    
    SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    SimpleDateFormat hr1 = new SimpleDateFormat("HH:mm:ss");    
    
    @Override
    @Transactional
    public TbRuta persistRuta(TbRuta ruta) {
        if(ruta!=null){
            Date today = new Date();
            if(ruta.getId()!=null){
                TbRuta toPersist = rutaRepository.findOne(ruta.getId());
                if(toPersist!=null){
                    ruta.setFechaModificacion(toPersist.getFechaModificacion());
                }
            }else{
                ruta.setIsPausa(0);
                ruta.setFechaCreacion(today);
                ruta.setFechaModificacion(today);
            }
            return rutaRepository.save(ruta);
        }
        return null;
    }


//    @Override
//    public ObjectListVO findByNameLike(int page, int maxResults, Map<String, Object> data) {
//        String querySelect = "SELECT t FROM TbPaquetes t WHERE t.idPaq IS NOT NULL ";
//        String queryCount = "SELECT COUNT(t) FROM TbPaquetes t WHERE t.idPaq IS NOT NULL";
//        String queryBuilder = "";
//        String noEmbarque = (String) data.get("noEmbarque");
//        if(noEmbarque!=null && !noEmbarque.isEmpty()){
//            queryBuilder +=" AND t.noEmbarque LIKE '%"+data.get("noEmbarque")+"%' ";
//        }
//        if(data.get("idOperador")!=null){
//            queryBuilder +=" AND t.idOperador.idUsu = "+data.get("idOperador");
//        }
//        if(data.get("idOrigen")!=null){
//            queryBuilder +=" AND t.idPuntoorigen.idPuntoe = "+data.get("idOrigen");
//        }
//        if(data.get("idEntrega")!=null){
//            queryBuilder +=" AND t.idPuntoe.idPuntoe = "+data.get("idEntrega");
//        }
//        if(data.get("usuario")!=null){
//            queryBuilder +=" AND t.idOperador.idUsu="+data.get("usuario");
//        }
//        
//        List<TbPaquetes> list = paqueteDao.getCustomQueryData(querySelect+queryBuilder,page*maxResults,maxResults);
//        Long total = paqueteDao.getCountCustomQueryData(queryCount+queryBuilder);
//        return new ObjectListVO(page, total, list);     
//    }

    @Override
    public TbRuta ivalidateRuta(Integer idRuta) {
        if(idRuta!=null && idRuta>0){
            TbRuta toPersist = rutaRepository.findOne(idRuta);
            toPersist.setEstatus(CREATE_INVALIDATE);
            return rutaRepository.save(toPersist);
        }
        return null;
    }

    @Override
    public TbRuta getRutaById(Integer id) {
        return rutaRepository.findOne(id);
    }

    @Override
    public List<TbBitacora> getListBitacoraByRuta(Integer idRuta) {
        return bitacoraRepository.getAllBitacorasByRuta(idRuta);
    }

    @Override
    public List<TbRuta> getListRutas(String idRuta) {
        String rutaString = "%"+idRuta+"%";
        return rutaRepository.getListRutaById(rutaString);
    }

    @Override
    public void processWallmartRutasFile(MultipartFile file, Integer tipoEnvio )throws Exception {            
//            String orgName = file.getOriginalFilename();
//            String filePath = csvfilePath + orgName;
//            File dest = new File(filePath);
//
//            file.transferTo(dest);
//            Map<String, int[]> columnsMap = new HashMap<String, int[]>();
//            columnsMap.put("store", new int[]{0});
//            columnsMap.put("ruta", new int[]{10});
//            columnsMap.put("operador", new int[]{12});
//            columnsMap.put("vehiculo", new int[]{13});
//            columnsMap.put("fecha", new int[]{14});
//            
//            List<Map<String, String>> result = fileProcesorExcel.getMapListFromXlsxAllString(filePath, 1,columnsMap);
//
//            List<String> listIdRutaB = new ArrayList<String>();
//            if(result!=null && !result.isEmpty()){
//                for(Map<String, String> item : result){
//                    if(item.get("ruta")!=null && !item.get("ruta").isEmpty()){
//                        if(!item.get("ruta").trim().equals("")){
//                            if(!listIdRutaB.contains(item.get("ruta"))){
//                                listIdRutaB.add(item.get("ruta"));
//                            }
//                        }                        
//                    }
//                }
//            }
//
//            
//            Map<String, List<BeanBitacoraRuta>> mapRutaBitacora = new HashMap<String, List<BeanBitacoraRuta>>();
//            
//            if(listIdRutaB!=null && !listIdRutaB.isEmpty()){
//                for(String idRuta: listIdRutaB){
//                    List<BeanBitacoraRuta> bitacoaList = new ArrayList<BeanBitacoraRuta>();
//                    for(Map<String, String> item : result){
//                        if(item.get("ruta")!=null && item.get("ruta").equals(idRuta)){
//                            BeanBitacoraRuta bean = new BeanBitacoraRuta(item.get("store"),item.get("operador"),item.get("vehiculo"),item.get("fecha"));
//                            bitacoaList.add(bean);
//                        }
//                    }
//                    mapRutaBitacora.put(idRuta, bitacoaList);
//                }
//            }
//            
//            persistServiceRutaBitacora(mapRutaBitacora, tipoEnvio);
    }

     @Transactional (rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    private void persistServiceRutaBitacora(Map<String, List<BeanBitacoraRuta>> mapRutaBitacora, Integer tipoEnvio) throws Exception{
            for(Map.Entry<String, List<BeanBitacoraRuta>> entry: mapRutaBitacora.entrySet()) {
                String rutaId = entry.getKey();
                List<BeanBitacoraRuta> listPuntos = entry.getValue();
                TbRuta ruta = createRuta(rutaId, tipoEnvio);
                //if(ruta!=null && ruta.getId()!=null){
                    for(int i=0;i<listPuntos.size();i++){
                        String origen = "";
                        String destino = "";
                        
                        BeanBitacoraRuta currentbean = listPuntos.get(i);
                        if(tipoEnvio==1){
                            if(i==0){
                                origen = "ORIGEN-GNKL";
                                destino = currentbean.getStore();
                            }else{
                                BeanBitacoraRuta beforebean = listPuntos.get(i-1);
                                origen = beforebean.getStore();
                                destino = currentbean.getStore();
                            }
                            if(origen!=null && !origen.isEmpty() && destino!=null && !destino.isEmpty()){
                                Integer isFinal = 0;
                                if(i==(listPuntos.size()-1)){
                                    isFinal = 1;
                                }
                                TbBitacora bitacora = createBitacora(ruta, destino, destino, origen, currentbean.getFecha(),currentbean.getOperador(),currentbean.getVehiculo(), isFinal);
                                if(currentbean.getOperador()!=null && !currentbean.getOperador().isEmpty()){
                                    sendMailBitacora(bitacora);
                                }                                
                            }                            
                        }else if(tipoEnvio==3){
                            if(i==0){
                                origen = "SENDERO-ENTREGAS";
                                destino = currentbean.getStore();
                            }else{
                                BeanBitacoraRuta beforebean = listPuntos.get(i-1);
                                origen = beforebean.getStore();
                                destino = currentbean.getStore();
                            }
                            if(origen!=null && !origen.isEmpty() && destino!=null && !destino.isEmpty()){
                                Integer isFinal = 0;
                                if(i==(listPuntos.size()-1)){
                                    isFinal = 1;
                                }
                                TbBitacora bitacora = createBitacora(ruta, destino, destino, origen, currentbean.getFecha(),currentbean.getOperador(),currentbean.getVehiculo(), isFinal);
                                if(currentbean.getOperador()!=null && !currentbean.getOperador().isEmpty()){
                                    sendMailBitacora(bitacora);
                                }
                            }                                            
                        }

                    }
//                    if(tipoEnvio==1){
//                        BeanBitacoraRuta currentbean = listPuntos.get(listPuntos.size()-1);
//                        //String ultimoPunto = listPuntos.get(listPuntos.size()-1).getStore();
//                        TbBitacora bitacora = createBitacora(ruta, "SENDERO_"+ruta.getNombre(), "SENDERO-ENTREGAS", currentbean.getStore(), currentbean.getFecha(),currentbean.getOperador(),currentbean.getVehiculo());
//                    }
                //}
            }         
    }
    

    @Transactional(propagation = Propagation.REQUIRED)    
    private TbRuta createRuta(String nombreRuta, Integer tipo){
        TbRuta rutaTest = new TbRuta();
        rutaTest.setEstatus(1);
        rutaTest.setFechaCreacion(new Date());
        rutaTest.setFechaModificacion(new Date());
        //TODO definir si esto va a ser recoleccion o entrega por medio de un combo en la carga del archivo
        TbTipoenvio tipoEnvio = tipoEnvioRepository.findOne(tipo);
        rutaTest.setIdEnvio(tipoEnvio);
        rutaTest.setNombre(nombreRuta);
        rutaTest.setIsPausa(0);
        //rutaDao.persistRuta(rutaTest);
        return rutaRepository.save(rutaTest);
        //return rutaTest;
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    private TbBitacora createBitacora(TbRuta ruta, String nombreRuta, String ptoDestino, String ptoOrigen, String fecha, String operador, String vehiculo, Integer isFinal) throws Exception{
        SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat mysqldt = new SimpleDateFormat("yyyy-MM-dd");
        Date today=new Date();
        TbBitacoraStatus status = bitacoraStatusRepository.getStatusByTipoEnvioNoEstatus(ruta.getIdEnvio().getIdEnvio(), 1);
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
        TbUsuarios usuarioLogged = usuarioRepository.findOne(user.getIdUsu());
        TbPuntoEo puntoOrigen = puntoRepository.getPuntoEntregaByNameEstatus(ptoOrigen);
        TbPuntoEo puntoDestino = puntoRepository.getPuntoEntregaByNameEstatus(ptoDestino);        
        //TODO poner el usuario por designar en las propiedades del sistema
        TbUsuarios operadorUsr = null;
        if(operador!=null && !operador.isEmpty()){
            operadorUsr = usuarioRepository.findDataByUsuario(operador);
            //TbUsuarios operadorUsr = usuarioRepository.findOne(6);
        } else {
            operadorUsr = usuarioRepository.findOne(6);
        }
        
        if(operadorUsr==null){
            operadorUsr = usuarioRepository.findOne(6);
        }

        //TODO poner el vehiculo por designar en las propiedades del sistema
        TbVehiculo vehiculoObj = null;
        if(vehiculo!=null && !vehiculo.isEmpty()){
            vehiculoObj = vehiculoRepository.getVehiculoByNoEconomico(vehiculo);
        } else {
            vehiculoObj = vehiculoRepository.findOne(9);
        }
        
        if(vehiculoObj==null){
            vehiculoObj = vehiculoRepository.findOne(9);
        }

        Date fechaRecepcion = dt.parse(fecha);
        
        TbBitacora bitacoraNew = new TbBitacora();
        bitacoraNew.setFecha(dt.parse(fecha));
        bitacoraNew.setFechaFin(null);
        bitacoraNew.setFechaCreacion(new Date());
        bitacoraNew.setIdEnvio(ruta.getIdEnvio());
        bitacoraNew.setIdOperador(operadorUsr);
        bitacoraNew.setIdRuta(ruta);
        bitacoraNew.setIdStatus(status);
        bitacoraNew.setIdUsuario(usuarioLogged);
        bitacoraNew.setIdVehiculo(vehiculoObj);
        bitacoraNew.setNombre(nombreRuta+" "+puntoDestino.getDireccion());
        bitacoraNew.setPtoDestino(puntoDestino);
        bitacoraNew.setPtoOrigen(puntoOrigen);

//        System.out.println(ptoDestino);
//        System.out.println(ptoOrigen);
//        
//        System.out.println(puntoDestino);
//        System.out.println(puntoOrigen);
        
        bitacoraNew.setKmInicial(0);
        bitacoraNew.setKmFinal(0);
        bitacoraNew.setIsFinal(isFinal);
        bitacoraNew.setIsPausa(0);
        bitacoraNew.setActivo(1);
        
        return bitacoraRepository.save(bitacoraNew);        
    }
    
    public void sendMailBitacora(TbBitacora result){
        TbUsuarios operador = result.getIdOperador();
        String bodyemail = " Se creó la siguiente bitácora: <br>"
                + " <br> Id Bitácora: " + result.getId() + "<br>"
                + " <br> Fecha creación: " + dt1.format(result.getFechaCreacion()) + " <br> "
                + " <br> Fecha recepción: " + dt1.format(result.getFecha()) + " <br> "
                + " <br> Punto Origen: " + result.getPtoOrigen().getNombrePunto() + "<br>"
                + " <br> Dirección Punto Origen: " + result.getPtoOrigen().getDireccion() + "<br>"
                + " <br> Punto Destino: " + result.getPtoDestino().getNombrePunto() + "<br>"
                + " <br> Dirección Punto Destino: " + result.getPtoDestino().getDireccion() + "<br>"
                + " <br> Operador: " + (operador.getNombre() != null ? operador.getNombre() : "") + " " + (operador.getApellidoPat() != null ? operador.getApellidoPat() : "") + " " + (operador.getApellidoMat() != null ? operador.getApellidoMat() : "") + "<br>"
                + " <br> Vehículo: " + result.getIdVehiculo().getNoEconomico()+"  "+result.getIdVehiculo().getPlacas()+" "+result.getIdVehiculo().getMarca() + "<br>"
                + " <br> Tipo: " + result.getIdEnvio().getDesEnvio() + "<br>";
        TbDomainConfig config = configRepository.findOne("gnk.mails");
        //mailer.sendTemplateMail(result.getIdOperador().getEmail(), "Bitácora creada", bodyemail, config.getConfigValue(), "");
        //mailer.sendTemplateMail(result.getIdOperador().getEmail(), result.getNombre(), bodyemail, config.getConfigValue(), "");        
    }
    
//    @Override
//    public ObjectListVO searchByParameters(String idRuta, String idStatus, Integer page, Integer maxResults) {
//        List<TbRuta> list = bitacoraDao.searchByParametersInfo(fechaInicial, fechaFinal, noTracking, idBitacora,idEnvio, page, maxResults);
//        //return new ObjectListVO(page, list.size(), list);   
//    }    

    @Override
    public List<TbRuta> getRutasListById(String idRuta, Integer status) {
        if(status!=null){
            return rutaRepository.getListByRutaNombre(idRuta, status);
        }else{
            return rutaRepository.getListByRutaNombre(idRuta);
        }
    }

    @Override
    public List<TbRuta> getRutasListByName(String idRuta) {
        idRuta = "%"+idRuta+"%";
        return rutaRepository.getListByRutaNombre(idRuta);
    }

    @Override
    public TbRutaInterrupcion setRutaInterrupcion(Integer idRuta, String comentario) {        
        TbRuta rutaFound = rutaRepository.findOne(idRuta);
        TbRutaInterrupcion interrupcion = new TbRutaInterrupcion();
        if(rutaFound!=null && rutaFound.getId()!=null){
            interrupcion = interrupcionRepository.getRutaInterrupcionByIdRuta(idRuta);
            if(interrupcion!=null && interrupcion.getId()!=null){
                interrupcion.setFechaFin(new Date());
                //interrupcion.setObservaciones(comentario);
                interrupcion.setStatus(1);// el proceso de interrupcion ya fue terminado
                interrupcion = interrupcionRepository.save(interrupcion);
                rutaFound.setIsPausa(0);// 0 la ruta no esta en pausa
                rutaFound = rutaRepository.save(rutaFound);
            }else{//significa que no tenemos registro de la interrupcion bajo la consulta anterior
                interrupcion = new TbRutaInterrupcion();
                interrupcion.setFechaInicio(new Date());
                interrupcion.setIdRuta(rutaFound);
                interrupcion.setObservaciones(comentario);
                interrupcion.setStatus(0);//0 es en proceso
                interrupcion = interrupcionRepository.save(interrupcion);
                rutaFound.setIsPausa(1);// 1 la ruta esta en pausa
                rutaFound = rutaRepository.save(rutaFound);
            }
            

            List<Map<String, Object>> mapList = rutaDao.getListBitacoraToBePause(idRuta, rutaFound.getIdEnvio().getIdEnvio());
            for(Map<String, Object> item : mapList){
                TbBitacora selectedBitacora = bitacoraRepository.findOne((Integer) item.get("id"));
                selectedBitacora.setIsPausa(rutaFound.getIsPausa());
                bitacoraRepository.save(selectedBitacora);
            }
            
            return interrupcion;
        }
                
        return new TbRutaInterrupcion();         
    }

    @Override
    public List<Map<String, Object>> getListTimesRutasByIdRuta(Integer idRuta) {
        return rutaDao.getTotalTimeByRuta(idRuta);
    }

    @Override
    public List<Map<String, Object>> getListTimesRutasByIdRuta(Integer offset, Integer page) {
        return rutaDao.getTotalTimeByRuta(0, 10);
    }
    
    
    @Override //TODO add posibility to add more files 
    public TbRuta saveRutaImagen(Integer idRuta, MultipartFile file) throws IOException {
        TbRuta ruta = rutaRepository.findOne(idRuta);
        TbArchivos archivo = new TbArchivos();
        archivo.setPathArchivo(persistFileToDirectory(file, idRuta));
        ruta.getArchivos().add(archivo);
        return rutaRepository.save(ruta);
    }
    
    @Override
    public List<TbBitacora> getActualBitacoraByRuta(Integer idRuta){
        PageRequest page = new PageRequest(0,1);
        List<TbBitacora> result = bitacoraRepository.getActualNotFinishBitacoraByRuta(idRuta,page);
        if(result!=null && result.isEmpty()){
           result = bitacoraRepository.getActualFechaFinFinishBitacoraByRuta(idRuta,page);
        }
        return result;
    }
    
    private String persistFileToDirectory(MultipartFile file, Integer id)throws IOException{
        InputStream inputStream = null;
        OutputStream outputStream = null;
                
        String fileName = file.getOriginalFilename();
        File newFile = new File(pathRuta+ id +"/"+fileName);
        
        File newdirectory = new File(pathRuta+ id);
        inputStream = file.getInputStream();
        if(!newdirectory.exists()){
            newdirectory.mkdir();
        }

        if (!newFile.exists()) {
            newFile.createNewFile();
        }
        outputStream = new FileOutputStream(newFile);
        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        return newFile.getPath();
    }    
}
