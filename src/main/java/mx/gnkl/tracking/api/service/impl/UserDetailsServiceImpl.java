package mx.gnkl.tracking.api.service.impl;

import mx.gnkl.tracking.api.bean.SecurityUserBean;
import mx.gnkl.tracking.api.model.TbRoles;
import mx.gnkl.tracking.api.model.TbUsuarios;
import mx.gnkl.tracking.api.persistence.jpa.RegistroEntradaRepository;
import mx.gnkl.tracking.api.persistence.jpa.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{
    @Autowired
    private UserRepository userRepository;  
    
//    @Autowired
//    private SessionRegistry sessionRegistry;
    
    @Autowired
    private RegistroEntradaRepository registroRepository;
    
    @Autowired
    private HttpServletRequest context;    
    
    @Override
    @Transactional()
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        TbUsuarios user = userRepository.findDataByUsuario(username);
        if(user!=null){
//            expireUserSessions(username);
//            addRegistroEntrada(user);
            Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
            String tipo="";
            for (TbRoles role : user.getRoles()){
                tipo+=role.getDesRol()+" ";
                grantedAuthorities.add(new SimpleGrantedAuthority(role.getDesRol()));
            }
            
            String usuario =user.getNombre()+" "+user.getApellidoPat()+" "+user.getApellidoMat();
            return new SecurityUserBean(user.getUsuario(), user.getPass(), grantedAuthorities, user.getIdUsu(), user.getEmail(),usuario.replace("null", "").toUpperCase(),tipo.replace("ROLE_", ""));
        }else{
            return new SecurityUserBean(null, null, null, null);
        }
    }
    
//    private void expireUserSessions(String username) {
//        for (Object principal : sessionRegistry.getAllPrincipals()) {
//            if (principal instanceof SecurityUserBean) {
//                UserDetails userDetails = (UserDetails) principal;
//                if (userDetails.getUsername().equals(username)) {
//                    for (SessionInformation information : sessionRegistry.getAllSessions(userDetails, true)) {
//                        information.expireNow();
//                    }
//                }
//            }
//        }    
//    }
    
//    private void addRegistroEntrada( TbUsuarios user ){
//            TbRegistroentrada registro = new TbRegistroentrada();
//            registro.setFechaHora(new Date());
//            registro.setUsuario(user.getUsuario());
//            String tipo = "";
//            for(TbRoles role : user.getRoles()){
//                tipo +=role.getDesRol()+" ";
//            }
//            registro.setTipo(tipo);
//            registroRepository.save(registro);        
//    }
    
    

    
}
