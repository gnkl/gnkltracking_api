/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.service;

import mx.gnkl.tracking.api.model.TbPuntoEo;
import java.util.List;

/**
 *
 * @author jmejia
 */
public interface PuntoDao {
    
    public List<TbPuntoEo> getCustomQueryData(String query, Integer offset, Integer count);
    
    public Long getCountCustomQueryData(String query);
    
    public void persistPunto(TbPuntoEo punto);
    
    public List<String> getDistinctGrupo(Integer entrega);
    
    public List<Integer> getDistinctIdPuntoEntrega(String grupo);
    
    public List<String> getClientesByGrupoPunto(String grupo);
    
    public List<Integer> getDistinctIdPuntoEntrega(String grupo, String cliente);
    
}
