/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.persistence.dao;

import mx.gnkl.tracking.api.model.TbRuta;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jmejia
 */
public interface RutaDao {    
    
    public void persistRuta(TbRuta ruta);
    
    public List<TbRuta> searchByParametersInfo(String idRuta, String status, Integer offset, Integer count);
    
    public List<Map<String,Object>> getTotalTimeByRuta(Integer idRuta);
    
    public List<Map<String, Object>> getTotalTimeByRuta(Integer page, Integer offset);
    
    public List<Map<String, Object>> getListBitacoraToBePause(Integer idRuta, Integer idEnvio);
}
