/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.persistence.jpa;

import mx.gnkl.tracking.api.model.TbBitacora;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface BitacoraRepository extends JpaRepository<TbBitacora, Integer> {
    
//    @Query(value="SELECT bit FROM TbBitacora bit LEFT JOIN bit.idOperador ope LEFT JOIN bit.ptoOrigen LEFT JOIN bit.idUsuario LEFT JOIN bit.idStatus LEFT JOIN bit.ptoDestino WHERE bit.idOperador.idUsu=:usuario AND bit.idStatus.numStatus NOT IN (:status) ")    
//    List<TbBitacora> getAllBitacorasByOperador(@Param("usuario") Integer usuario, @Param("status") List<Integer> statusList ,Pageable pageable);
    @Query(value="SELECT bit FROM TbBitacora bit WHERE bit.idOperador.idUsu=:usuario AND bit.idStatus.numStatus NOT IN (:status) AND bit.isFinal=:isfinal ORDER BY bit.id ASC")    
    List<TbBitacora> getAllBitacorasByOperadorFinal(@Param("usuario") Integer usuario, @Param("status") List<Integer> statusList , @Param("isfinal") Integer isFinal, Pageable pageable);

//    @Query(value="SELECT bit FROM TbBitacora bit WHERE bit.idOperador.idUsu=:usuario AND bit.idStatus.numStatus NOT IN (:status) ORDER BY bit.id ASC")    
//    List<TbBitacora> getAllBitacorasByOperadorFinal(@Param("usuario") Integer usuario, @Param("status") List<Integer> statusList , Pageable pageable);    

    @Query(value="SELECT bit FROM TbBitacora bit WHERE bit.idOperador.idUsu=:usuario AND bit.idStatus.numStatus NOT IN (:status) AND bit.idEnvio.idEnvio=:tipoEnvio ORDER BY bit.id ASC")    
    List<TbBitacora> getAllBitacorasByOperadorFinalTipoEnvio(@Param("usuario") Integer usuario, @Param("status") List<Integer> statusList , @Param("tipoEnvio") Integer tipoEnvio, Pageable pageable);    

    @Query(value="SELECT bit FROM TbBitacora bit WHERE bit.idOperador.idUsu=:usuario AND bit.idStatus.numStatus NOT IN (:status) AND bit.idEnvio.idEnvio=:tipoEnvio ORDER BY bit.id ASC")    
    List<TbBitacora> getAllBitacorasByOperadorFinalTipoEnvio(@Param("usuario") Integer usuario, @Param("status") List<Integer> statusList , @Param("tipoEnvio") Integer tipoEnvio);        
    
//    @Query(value=" SELECT * FROM tb_bitacora tbb, tb_bitacora_status stat WHERE tbb.id_operador=:usuario AND tbb.id_status=stat.id AND stat.num_status NOT IN (:statusFinal) AND tbb.is_final=1 UNION SELECT * FROM tb_bitacora tbb, tb_bitacora_status stat WHERE tbb.id_operador=:usuario AND tbb.id_status=stat.id AND stat.num_status NOT IN (:statusNoFinal) AND tbb.is_final=0 ",nativeQuery = true)
//    @Query(value=" SELECT * FROM tb_bitacora tbb, tb_bitacora_status stat WHERE tbb.id_operador=:usuario AND tbb.id_status=stat.id AND stat.num_status NOT IN (:statusFinal) AND tbb.is_final=1 ",nativeQuery = true)
//    List<TbBitacora> getAllBitacorasByOperadorUnion(@Param("usuario") Integer usuario, @Param("statusFinal") List<Integer> statusListFinal , @Param("statusNoFinal") List<Integer> statusList);
    
    @Query(value="SELECT bit FROM TbBitacora bit LEFT JOIN bit.idOperador ope LEFT JOIN bit.ptoOrigen LEFT JOIN bit.idUsuario LEFT JOIN bit.idStatus LEFT JOIN bit.ptoDestino WHERE bit.idOperador.idUsu=:usuario")    
    List<TbBitacora> getAllBitacorasByOperador(@Param("usuario") Integer usuario);    

    @Query(value="SELECT count(bit) FROM TbBitacora bit LEFT JOIN bit.idOperador ope LEFT JOIN bit.ptoOrigen LEFT JOIN bit.idUsuario LEFT JOIN bit.idStatus LEFT JOIN bit.ptoDestino WHERE bit.idOperador.idUsu=:usuario")    
    Integer getCountBitacorasByOperador(@Param("usuario") Integer usuario);

    @Query(value="SELECT count(bit) FROM TbBitacora bit LEFT JOIN bit.idOperador ope LEFT JOIN bit.ptoOrigen LEFT JOIN bit.idUsuario LEFT JOIN bit.idStatus LEFT JOIN bit.ptoDestino WHERE bit.idOperador.idUsu=:usuario AND bit.idEnvio.idEnvio=:tipoEnvio ")    
    Integer getCountBitacorasByOperadorTipoEnvio(@Param("usuario") Integer usuario, @Param("tipoEnvio") Integer tipoEnvio);
    
    @Query(value="SELECT bit FROM TbBitacora bit LEFT JOIN bit.idOperador ope LEFT JOIN bit.ptoOrigen LEFT JOIN bit.idUsuario LEFT JOIN bit.idStatus LEFT JOIN bit.ptoDestino WHERE bit.idUsuario.idUsu=:usuario")    
    List<TbBitacora> getAllBitacorasByUsuario(@Param("usuario") Integer usuario, Pageable pageable);
    
    @Query(value="SELECT bit FROM TbBitacora bit LEFT JOIN bit.idOperador ope LEFT JOIN bit.ptoOrigen LEFT JOIN bit.idUsuario LEFT JOIN bit.idStatus LEFT JOIN bit.ptoDestino WHERE bit.idUsuario.idUsu=:usuario")    
    List<TbBitacora> getAllBitacorasByUsuario(@Param("usuario") Integer usuario);    
        
    @Query(value=" SELECT b.* FROM tb_bitacora b where date(b.fecha)=date(:fecha) and id_operador=:idoperador and b.id not in (select id_bitacora from tb_bitacora_paquete where id_bitacora=b.id) ORDER BY id DESC LIMIT 1 ", nativeQuery = true)
    TbBitacora getBitacoraByDate(@Param("fecha") String fecha, @Param("idoperador") Integer idOperador);

    @Query(value=" SELECT b.* FROM tb_bitacora b where date(b.fecha)=date(:fecha) and id_operador=:idoperador and b.id_status not in (4,5) and id_envio=:idEnvio ORDER BY id DESC LIMIT 1 ", nativeQuery = true)
    TbBitacora getBitacoraByDate(@Param("fecha") String fecha, @Param("idoperador") Integer idOperador, @Param("idEnvio") Integer idEnvio);
    
    @Query(value=" SELECT b.* FROM tb_bitacora b where date(b.fecha)=date(:fecha) and b.id not in (select id_bitacora from tb_bitacora_paquete where id_bitacora=b.id) ORDER BY id DESC LIMIT 1 ", nativeQuery = true)
    TbBitacora getBitacoraByDate(@Param("fecha") String fecha);    
    
    @Query(value="SELECT bit FROM TbBitacora bit  WHERE bit.idRuta.id=:idruta ORDER BY id ASC")    
    List<TbBitacora> getAllBitacorasByRuta(@Param("idruta") Integer idRuta);
    
    @Query(value="SELECT * FROM tb_bitacora bit WHERE bit.id=:idbitacora",nativeQuery = true)    
    List<TbBitacora> getListByIdBitacora(@Param("idbitacora") String idBitacora);

    @Query(value="SELECT * FROM tb_bitacora bit  WHERE bit.nombre LIKE :nombre", nativeQuery = true)    
    List<TbBitacora> getListByBitacoraNombre(@Param("nombre") String nombre);
    
    @Query(value="SELECT * FROM tb_bitacora bit WHERE bit.id_ruta=:idruta AND bit.is_final=1",nativeQuery = true)    
    List<TbBitacora> getListBitacoraFinalByRuta(@Param("idruta") Integer idRuta);

    @Query(value="SELECT MAX(id) FROM tb_bitacora bit WHERE bit.id_ruta=:idruta",nativeQuery = true)    
    Integer getMaxIdBitacoraByRuta(@Param("idruta") Integer idRuta);    
    
    @Query(value="SELECT bit FROM TbBitacora bit WHERE bit.idStatus.numStatus NOT IN (:status) AND bit.idEnvio.idEnvio=:tipoEnvio ORDER BY bit.id ASC")    
    List<TbBitacora> getAllBitacorasForCliente(@Param("status") List<Integer> statusList, @Param("tipoEnvio") Integer idEnvio, Pageable pageable);

    @Query(value="SELECT count(bit) FROM TbBitacora bit WHERE bit.idStatus.numStatus NOT IN (:status) AND bit.idEnvio.idEnvio=:tipoEnvio")    
    Long getCountAllBitacorasForCliente(@Param("status") List<Integer> statusList, @Param("tipoEnvio") Integer idEnvio);    
    
    @Query(value="SELECT bit FROM TbBitacora bit WHERE bit.idStatus.numStatus IN (:listStatus) AND bit.idEnvio.idEnvio=:tipoEnvio ORDER BY bit.id ASC")    
    List<TbBitacora> getAllBitacorasByStatusEnvio(@Param("listStatus") List<Integer> listStatus, @Param("tipoEnvio") Integer idEnvio);
    
    @Query(value=" SELECT bit FROM TbBitacora bit WHERE bit.idRuta.id=:idRuta AND bit.idStatus.numStatus>1 AND bit.fechaFin IS NULL")
    List<TbBitacora> getActualNotFinishBitacoraByRuta(@Param("idRuta") Integer idRuta, Pageable pageable);

    @Query(value=" SELECT bit FROM TbBitacora bit WHERE bit.idRuta.id=:idRuta AND bit.idStatus.numStatus>1 AND bit.fechaFin IS NOT NULL ORDER BY bit.fechaFin DESC")
    List<TbBitacora> getActualFechaFinFinishBitacoraByRuta(@Param("idRuta") Integer idRuta, Pageable pageable);    
}
