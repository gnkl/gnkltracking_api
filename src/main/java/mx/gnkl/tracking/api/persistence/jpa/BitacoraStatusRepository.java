/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.persistence.jpa;

import mx.gnkl.tracking.api.model.TbBitacoraStatus;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface BitacoraStatusRepository extends JpaRepository<TbBitacoraStatus, Integer>{
    
    @Query(value="SELECT stat FROM TbBitacoraStatus stat WHERE stat.idEmpresa=:empresa AND stat.numStatus=:noestatus")
    TbBitacoraStatus getStatusByEmpresaNoEstatus(@Param("empresa") Integer empresa, @Param("noestatus") Integer noestatus);

    @Query(value="SELECT stat FROM TbBitacoraStatus stat WHERE stat.idEmpresa=:empresa AND stat.numStatus=:noestatus AND stat.idEnvio.idEnvio=:idEnvio")
    TbBitacoraStatus getStatusByEmpresaNoEstatusEnvio(@Param("empresa") Integer empresa, @Param("noestatus") Integer noestatus, @Param("idEnvio") Integer idEnvio);    
    
    @Query(value="SELECT stat FROM TbBitacoraStatus stat WHERE stat.idEnvio.idEnvio=:idenvio AND stat.numStatus=:noestatus")
    TbBitacoraStatus getStatusByTipoEnvioNoEstatus(@Param("idenvio") Integer idEnvio, @Param("noestatus") Integer noestatus);    
    
    @Query(value="SELECT stat.id FROM TbBitacoraStatus stat WHERE stat.idEnvio.idEnvio=:idenvio AND stat.numStatus IN (:noestatus)")
    List<Integer> getStatusListByTipoEnvioNoEstatus(@Param("idenvio") Integer idEnvio, @Param("noestatus") List<Integer> noestatus);    
    
}
