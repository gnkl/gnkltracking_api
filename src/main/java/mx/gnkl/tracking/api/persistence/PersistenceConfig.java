package mx.gnkl.tracking.api.persistence;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableTransactionManagement
//@PropertySource({ "classpath:persistence-${envTarget:mysql}.properties" })
@PropertySource(value = { "classpath:application.properties" })
@ComponentScan({ "mx.gnkl.tracking.api.persistence" })
@EnableJpaRepositories(basePackages = "mx.gnkl.tracking.api.persistence.jpa")
public class PersistenceConfig {

    @Autowired
    private Environment env;

    public PersistenceConfig() {
        super();
    }

    /*@Bean(name = "dataSource",destroyMethod = "close")
    public DataSource dataSource() throws PropertyVetoException {
        final com.mchange.v2.c3p0.ComboPooledDataSource dataSource = new com.mchange.v2.c3p0.ComboPooledDataSource();
        dataSource.setDriverClass("com.mysql.jdbc.Driver");
        dataSource.setJdbcUrl("jdbc:mysql://189.194.249.164:3306/tracking_wallmart");
        //dataSource.setJdbcUrl("jdbc:mysql://192.168.0.50:3306/tracking_wallmart");
        dataSource.setUser("root");
        dataSource.setPassword("eve9397");
        dataSource.setNumHelperThreads(10);
        dataSource.setMaxAdministrativeTaskTime(10000);
        dataSource.setInitialPoolSize(3);
        dataSource.setMinPoolSize(10);
        dataSource.setMaxPoolSize(35);
        dataSource.setAcquireIncrement(3);
        dataSource.setMaxStatements(0);
        dataSource.setAcquireRetryAttempts(30);
        dataSource.setAcquireRetryDelay(1000);
        dataSource.setBreakAfterAcquireFailure(false);
        
        dataSource.setMaxIdleTime(180);
        dataSource.setMaxConnectionAge(300);
        dataSource.setCheckoutTimeout(5000);
        dataSource.setIdleConnectionTestPeriod(60);
        dataSource.setTestConnectionOnCheckout(true);
        dataSource.setTestConnectionOnCheckin(true);
        dataSource.setPreferredTestQuery("select 1 from DUAL");
        return dataSource;
    }*/

@Bean(name = "dataSource",destroyMethod = "close")
    public DataSource dataSource() throws PropertyVetoException {
        final com.mchange.v2.c3p0.ComboPooledDataSource dataSource = new com.mchange.v2.c3p0.ComboPooledDataSource();
        dataSource.setDriverClass(env.getProperty("jdbc.driverClassName"));
        dataSource.setJdbcUrl(env.getProperty("jdbc.url"));
        dataSource.setUser(env.getProperty("jdbc.username"));
        dataSource.setPassword(env.getProperty("jdbc.password"));
        dataSource.setNumHelperThreads(10);
        dataSource.setMaxAdministrativeTaskTime(10000);
        dataSource.setInitialPoolSize(3);
        dataSource.setMinPoolSize(10);
        dataSource.setMaxPoolSize(35);
        dataSource.setAcquireIncrement(3);
        dataSource.setMaxStatements(0);
        dataSource.setAcquireRetryAttempts(30);
        dataSource.setAcquireRetryDelay(1000);
        dataSource.setBreakAfterAcquireFailure(false);
        
        dataSource.setMaxIdleTime(180);
        dataSource.setMaxConnectionAge(300);
        dataSource.setCheckoutTimeout(5000);
        dataSource.setIdleConnectionTestPeriod(60);
        dataSource.setTestConnectionOnCheckout(true);
        dataSource.setTestConnectionOnCheckin(true);
        dataSource.setPreferredTestQuery("select 1 from DUAL");
        return dataSource;
    }    
    
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws PropertyVetoException{
       LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
       entityManagerFactory.setDataSource(dataSource());
       entityManagerFactory.setPackagesToScan("mx.gnkl.tracking.api.model");
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        // vendorAdapter.set
        entityManagerFactory.setJpaVendorAdapter(vendorAdapter);
        entityManagerFactory.setJpaProperties(additionalProperties());

       return entityManagerFactory;
    }
    
    @Bean
    public JpaTransactionManager transactionManager() throws PropertyVetoException {
        final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    final Properties additionalProperties() {
        final Properties hibernateProperties = new Properties();
        
        hibernateProperties.setProperty("hibernate.dialect","org.hibernate.dialect.MySQL5Dialect");
        hibernateProperties.setProperty("hibernate.show_sql","true");
        
        return hibernateProperties;
    }

}