/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.persistence.jpa;

import mx.gnkl.tracking.api.model.TbDomainConfig;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author jmejia
 */
public interface DomainConfigRepository extends JpaRepository<TbDomainConfig, String>{
    
}
