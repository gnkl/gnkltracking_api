/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.persistence.jpa;

import mx.gnkl.tracking.api.model.TbVehiculo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface VehiculoRepository extends JpaRepository<TbVehiculo, Integer>{
    
    @Query("SELECT t FROM TbVehiculo t WHERE t.noEconomico=:noEconomico")
    TbVehiculo getVehiculoByNoEconomico(@Param("noEconomico") String noEconomico);    
}
