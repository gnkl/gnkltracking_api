/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.persistence.jpa;

import mx.gnkl.tracking.api.model.TbBitacoraHistorico;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface BitacoraHistoricoRepository extends JpaRepository<TbBitacoraHistorico, Integer> {
    
    @Query(value="SELECT hist FROM TbBitacoraHistorico hist WHERE hist.idBitacora.id=:bitacora")
    List<TbBitacoraHistorico> getAllHistoricoByBitacora(@Param("bitacora") Integer idBitacora);

    @Query(value="SELECT hist FROM TbBitacoraHistorico hist WHERE hist.idBitacora.id=:bitacora AND hist.estatus.numStatus NOT IN (:liststatus)")
    List<TbBitacoraHistorico> getAllHistoricoByBitacora(@Param("bitacora") Integer idBitacora, @Param("liststatus") List<Integer> listStatus);
    
    @Query(value="SELECT hist FROM TbBitacoraHistorico hist WHERE hist.estatus.id=:estatus")
    List<TbBitacoraHistorico> getAllHistoricoByEstatus(@Param("estatus") Integer estatus);

    @Query(value="SELECT hist FROM TbBitacoraHistorico hist WHERE hist.estatus.id=:estatus AND hist.idBitacora.id=:idBitacora")
    TbBitacoraHistorico getHistoricoByEstatusBitacora(@Param("estatus") Integer estatus, @Param("idBitacora")Integer idBitacora);    
    
    @Query(value="SELECT MAX(hist.kilometraje) FROM TbBitacoraHistorico hist WHERE hist.idBitacora.id=:idBitacora")
    Integer getCountKmHistoricoByBitacora(@Param("idBitacora")Integer idBitacora);    
    
    @Query(value="SELECT hist FROM TbBitacoraHistorico hist WHERE hist.idBitacora.id=:bitacora AND hist.estatus.numStatus IN (:liststatus) ORDER BY hist.id ASC")
    List<TbBitacoraHistorico> getAllHistoricoByBitacoraInEstatus(@Param("bitacora") Integer idBitacora, @Param("liststatus") List<Integer> listStatus);    
}
