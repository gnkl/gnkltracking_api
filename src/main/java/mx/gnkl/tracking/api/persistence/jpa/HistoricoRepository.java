package mx.gnkl.tracking.api.persistence.jpa;

import mx.gnkl.tracking.api.model.TbHistpaq;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HistoricoRepository extends JpaRepository<TbHistpaq, Integer> {

    @Query(value="SELECT paq FROM TbHistpaq paq LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPaq")
    List<TbHistpaq> getAllPaquetes(Pageable pageable);
    
    @Query(value="SELECT hist FROM TbHistpaq hist LEFT JOIN hist.idUsu LEFT JOIN hist.idStatus LEFT JOIN hist.idPaq WHERE hist.idPaq.idPaq=:idPaq")
    List<TbHistpaq> getAllPaquetesByIdPaq(@Param("idPaq") Integer idPaq, Pageable pageable);

    @Query(value="SELECT hist FROM TbHistpaq hist LEFT JOIN hist.idUsu LEFT JOIN hist.idStatus LEFT JOIN hist.idPaq WHERE hist.idPaq.idPaq=:idPaq AND hist.idStatus.numStatus!=1")
    List<TbHistpaq> getAllPaquetesByIdPaqStatus(@Param("idPaq") Integer idPaq, Pageable pageable);    
    
    @Query(value="SELECT hist FROM TbHistpaq hist LEFT JOIN hist.idUsu LEFT JOIN hist.idStatus LEFT JOIN hist.idPaq paq WHERE paq.noEmbarque=:noEmbarque")
    List<TbHistpaq> getAllPaquetesByNoEmbarque(@Param("noEmbarque") String noEmbarque, Pageable pageable);

    @Query(value="SELECT hist FROM TbHistpaq hist LEFT JOIN hist.idUsu usu LEFT JOIN hist.idStatus LEFT JOIN hist.idPaq paq WHERE paq.noEmbarque=:noEmbarque AND paq.idUsu.idUsu=:idusu")
    List<TbHistpaq> getAllPaquetesByNoEmbarqueIdUser(@Param("noEmbarque") String noEmbarque, @Param("idusu") Integer idusu, Pageable pageable);
    
    @Query(value="SELECT COUNT(hist) FROM TbHistpaq hist LEFT JOIN hist.idPaq paq WHERE paq.noEmbarque=:noEmbarque")
    Integer getCountAllPaquetesByNoEmbarque(@Param("noEmbarque") String noEmbarque);

    @Query(value="SELECT COUNT(hist) FROM TbHistpaq hist LEFT JOIN hist.idPaq paq WHERE hist.idPaq.idPaq=:idPaq")
    Integer getCountAllPaquetesByIdPaq(@Param("idPaq") Integer idPaq);    
    
    @Query(value="SELECT COUNT(hist) FROM TbHistpaq hist LEFT JOIN hist.idPaq paq WHERE paq.noEmbarque=:noEmbarque AND paq.idUsu.idUsu=:idusu")
    Integer getCountAllPaquetesByNoEmbarqueIdUser(@Param("noEmbarque") String noEmbarque, @Param("idusu") Integer idusu);
    
    @Query(value="SELECT hist FROM TbHistpaq hist INNER JOIN hist.idPaq paq INNER JOIN hist.idStatus stat WHERE paq.noEmbarque=:noEmbarque AND stat.numStatus!=:status")
    List<TbHistpaq> getAllPaquetesByNoEmbarqueStatus(@Param("noEmbarque") String noEmbarque, @Param("status") Integer status);    
    
    @Query(value="SELECT hist FROM TbHistpaq hist INNER JOIN hist.idPaq INNER JOIN hist.idStatus WHERE hist.idPaq.idPaq=:idpaq AND hist.idStatus.idStatus=:status")
    List<TbHistpaq> getAllPaquetesByIdPaqStatus(@Param("idpaq") Integer idPaq, @Param("status") Integer status);        
}
