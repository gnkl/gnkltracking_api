/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.persistence.jpa;

import mx.gnkl.tracking.api.model.TbPuntoEo;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface PuntoRepository extends JpaRepository<TbPuntoEo, Integer>{
    
    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.entrega = :entrega AND t.origen = :origen AND t.estatus = :estatus")
    public List<TbPuntoEo> getPuntoByTipoEstatusPageable(@Param("entrega") Integer entrega, @Param("origen") Integer origen, @Param("estatus") Integer estatus , Pageable page);

    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.entrega = :entrega AND t.origen = :origen")
    public List<TbPuntoEo> getPuntoByTipoPageable(@Param("entrega") Integer entrega, @Param("origen") Integer origen, Pageable page);

    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.entrega = :entrega AND t.origen = :origen AND t.estatus = :estatus AND t.nombrePunto LIKE :nombre")
    public List<TbPuntoEo> getPuntoByTipoEstatusPageable(@Param("entrega") Integer entrega, @Param("origen") Integer origen, @Param("estatus") Integer estatus , @Param("nombre") String nombre,Pageable page);

//    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.entrega = 0 AND t.origen = :origen AND t.estatus = :estatus AND t.nombrePunto LIKE :nombre")
//    public List<TbPuntoEo> getPuntoOrigenEstatusPageable(@Param("nombre") String nombre,Pageable page);
//
//    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.entrega = :entrega AND t.origen = :origen AND t.estatus = :estatus AND t.nombrePunto LIKE :nombre")
//    public List<TbPuntoEo> getPuntoEntregaEstatusPageable(@Param("entrega") Integer entrega, @Param("origen") Integer origen, @Param("estatus") Integer estatus , @Param("nombre") String nombre,Pageable page);
    
    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.estatus = :estatus")
    public List<TbPuntoEo> getPuntoByEstatusPageable(@Param("estatus") Integer estatus , Pageable page);

    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.estatus = 1 AND t.entrega = 1 ")
    public List<TbPuntoEo> getAllPuntoEntrega();

    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.estatus = 1 AND t.origen=1 ")
    public List<TbPuntoEo> getAllPuntoOrigen();

    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.estatus = :estatus AND t.entrega = 1 AND t.nombrePunto LIKE :nombre")
    public List<TbPuntoEo> getAllPuntoEntregaByNameEstatus(@Param("estatus") Integer estatus , @Param("nombre") String nombre);

    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.estatus = :estatus AND t.origen=1 AND t.nombrePunto LIKE :nombre")
    public List<TbPuntoEo> getAllPuntoOrigenByNameEstatus(@Param("estatus") Integer estatus , @Param("nombre") String nombre);
    
    
    @Query(value="SELECT COUNT(t) FROM TbPuntoEo t WHERE t.estatus = :estatus")
    public Long getCountPuntoByEstatus(@Param("estatus") Integer estatus);

    @Query(value="SELECT DISTINCT(t.nombrePunto) FROM TbPuntoEo t WHERE t.nombrePunto LIKE :nombre")
    public List<String> getAllPuntoByNameEstatus(@Param("nombre") String nombre);

    @Query(value="SELECT DISTINCT(t.direccion) FROM TbPuntoEo t WHERE t.direccion LIKE :nombre")
    public List<String> getAllPuntoByDireccionEstatus(@Param("nombre") String nombre);    

    @Query(value="SELECT t FROM TbPuntoEo t WHERE t.estatus = 1 AND t.nombrePunto=:nombre")
    public TbPuntoEo getPuntoEntregaByNameEstatus(@Param("nombre") String nombre);

    
}
