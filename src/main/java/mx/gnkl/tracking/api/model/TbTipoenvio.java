/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_tipoenvio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbTipoenvio.findAll", query = "SELECT t FROM TbTipoenvio t"),
    @NamedQuery(name = "TbTipoenvio.findByIdEnvio", query = "SELECT t FROM TbTipoenvio t WHERE t.idEnvio = :idEnvio"),
    @NamedQuery(name = "TbTipoenvio.findByDesEnvio", query = "SELECT t FROM TbTipoenvio t WHERE t.desEnvio = :desEnvio")})
public class TbTipoenvio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_envio")
    private Integer idEnvio;
    @Size(max = 45)
    @Column(name = "des_envio")
    private String desEnvio;

    public TbTipoenvio() {
    }

    public TbTipoenvio(Integer idEnvio) {
        this.idEnvio = idEnvio;
    }

    public Integer getIdEnvio() {
        return idEnvio;
    }

    public void setIdEnvio(Integer idEnvio) {
        this.idEnvio = idEnvio;
    }

    public String getDesEnvio() {
        return desEnvio;
    }

    public void setDesEnvio(String desEnvio) {
        this.desEnvio = desEnvio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEnvio != null ? idEnvio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbTipoenvio)) {
            return false;
        }
        TbTipoenvio other = (TbTipoenvio) object;
        if ((this.idEnvio == null && other.idEnvio != null) || (this.idEnvio != null && !this.idEnvio.equals(other.idEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbTipoenvio[ idEnvio=" + idEnvio + " ]";
    }
    
}
