/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_domain_config")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbDomainConfig.findAll", query = "SELECT t FROM TbDomainConfig t"),
    @NamedQuery(name = "TbDomainConfig.findByConfigName", query = "SELECT t FROM TbDomainConfig t WHERE t.configName = :configName"),
    @NamedQuery(name = "TbDomainConfig.findByConfigValue", query = "SELECT t FROM TbDomainConfig t WHERE t.configValue = :configValue"),
    @NamedQuery(name = "TbDomainConfig.findByConfigDesc", query = "SELECT t FROM TbDomainConfig t WHERE t.configDesc = :configDesc")})
public class TbDomainConfig implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "config_name")
    private String configName;
    @Basic(optional = false)
    @Column(name = "config_value")
    private String configValue;
    @Column(name = "config_desc")
    private String configDesc;

    public TbDomainConfig() {
    }

    public TbDomainConfig(String configName) {
        this.configName = configName;
    }

    public TbDomainConfig(String configName, String configValue) {
        this.configName = configName;
        this.configValue = configValue;
    }

    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    public String getConfigValue() {
        return configValue;
    }

    public void setConfigValue(String configValue) {
        this.configValue = configValue;
    }

    public String getConfigDesc() {
        return configDesc;
    }

    public void setConfigDesc(String configDesc) {
        this.configDesc = configDesc;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (configName != null ? configName.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbDomainConfig)) {
            return false;
        }
        TbDomainConfig other = (TbDomainConfig) object;
        if ((this.configName == null && other.configName != null) || (this.configName != null && !this.configName.equals(other.configName))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbDomainConfig[ configName=" + configName + " ]";
    }
    
}
