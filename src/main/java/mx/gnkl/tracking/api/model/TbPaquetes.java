/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_paquetes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPaquetes.findAll", query = "SELECT t FROM TbPaquetes t"),
    @NamedQuery(name = "TbPaquetes.findByIdPaq", query = "SELECT t FROM TbPaquetes t WHERE t.idPaq = :idPaq"),
    @NamedQuery(name = "TbPaquetes.findByNoEmbarque", query = "SELECT t FROM TbPaquetes t WHERE t.noEmbarque = :noEmbarque"),
    @NamedQuery(name = "TbPaquetes.findByFecCargaPaq", query = "SELECT t FROM TbPaquetes t WHERE t.fecCargaPaq = :fecCargaPaq"),
    @NamedQuery(name = "TbPaquetes.findByFechaEntPaq", query = "SELECT t FROM TbPaquetes t WHERE t.fechaEntPaq = :fechaEntPaq"),
    @NamedQuery(name = "TbPaquetes.findByFechaRecPaq", query = "SELECT t FROM TbPaquetes t WHERE t.fechaRecPaq = :fechaRecPaq")})
public class TbPaquetes implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_paq")
    private Integer idPaq;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "no_embarque")
    private String noEmbarque;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "fec_carga_paq")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecCargaPaq;
    
    @Basic(optional = false)
    @Column(name = "fecha_ent_paq")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaEntPaq;
    
    @Basic(optional = false)
    @Column(name = "fecha_rec_paq")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRecPaq;
    
    @JoinColumn(name = "id_operador", referencedColumnName = "id_usu")
    @ManyToOne()
    private TbUsuarios idOperador;
    
    @JoinColumn(name = "id_puntoorigen", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbPuntoEo idPuntoorigen;
    
    @JoinColumn(name = "id_usu", referencedColumnName = "id_usu")
    @ManyToOne(optional = false)
    private TbUsuarios idUsu;
    
    @JoinColumn(name = "id_status", referencedColumnName = "id_status")
    @ManyToOne(optional = false)
    private TbStatus idStatus;
    
    @JoinColumn(name = "id_puntoe", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbPuntoEo idPuntoe;
    
    @JoinColumn(name = "id_ruta", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private TbRuta idRuta;    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    
    @JoinColumn(name = "id_vehiculo", referencedColumnName = "id")
    @ManyToOne(optional = true)
    private TbVehiculo idVehiculo;    

    @Column(name = "no_tarimas")
    private Integer noTarimas;

    @Column(name = "fase")
    private Integer fase;    
    
    @Column(name = "cita")
    private Integer cita; 

    @Column(name = "no_cajas")
    private Integer noCajas;

    
    public TbPaquetes() {
    }

    public TbPaquetes(Integer idPaq) {
        this.idPaq = idPaq;
    }

    public TbPaquetes(Integer idPaq, String noEmbarque, Date fecCargaPaq) {
        this.idPaq = idPaq;
        this.noEmbarque = noEmbarque;
        this.fecCargaPaq = fecCargaPaq;
    }

    public Integer getIdPaq() {
        return idPaq;
    }

    public void setIdPaq(Integer idPaq) {
        this.idPaq = idPaq;
    }

    public String getNoEmbarque() {
        return noEmbarque;
    }

    public void setNoEmbarque(String noEmbarque) {
        this.noEmbarque = noEmbarque;
    }

    public Date getFecCargaPaq() {
        return fecCargaPaq;
    }

    public void setFecCargaPaq(Date fecCargaPaq) {
        this.fecCargaPaq = fecCargaPaq;
    }

    public Date getFechaEntPaq() {
        return fechaEntPaq;
    }

    public void setFechaEntPaq(Date fechaEntPaq) {
        this.fechaEntPaq = fechaEntPaq;
    }

    public Date getFechaRecPaq() {
        return fechaRecPaq;
    }

    public void setFechaRecPaq(Date fechaRecPaq) {
        this.fechaRecPaq = fechaRecPaq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPaq != null ? idPaq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPaquetes)) {
            return false;
        }
        TbPaquetes other = (TbPaquetes) object;
        if ((this.idPaq == null && other.idPaq != null) || (this.idPaq != null && !this.idPaq.equals(other.idPaq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbPaquetes[ idPaq=" + idPaq + " ]";
    }

////    /**
////     * @return the idOperador
////     */
////    public TbOperadores getIdOperador() {
////        return idOperador;
////    }
////
////    /**
////     * @param idOperador the idOperador to set
////     */
////    public void setIdOperador(TbOperadores idOperador) {
////        this.idOperador = idOperador;
////    }
////
////    /**
////     * @return the idPuntoorigen
////     */
////    public TbPuntosorigen getIdPuntoorigen() {
////        return idPuntoorigen;
////    }
////
////    /**
////     * @param idPuntoorigen the idPuntoorigen to set
////     */
////    public void setIdPuntoorigen(TbPuntosorigen idPuntoorigen) {
////        this.idPuntoorigen = idPuntoorigen;
////    }
////
////    /**
////     * @return the idUsu
////     */
////    public TbUsuarios getIdUsu() {
////        return idUsu;
////    }
////
////    /**
////     * @param idUsu the idUsu to set
////     */
////    public void setIdUsu(TbUsuarios idUsu) {
////        this.idUsu = idUsu;
////    }
////
////    /**
////     * @return the idStatus
////     */
////    public TbStatus getIdStatus() {
////        return idStatus;
////    }
////
////    /**
////     * @param idStatus the idStatus to set
////     */
////    public void setIdStatus(TbStatus idStatus) {
////        this.idStatus = idStatus;
////    }
////
////    /**
////     * @return the idPuntoe
////     */
////    public TbPuntosentrega getIdPuntoe() {
////        return idPuntoe;
////    }
////
////    /**
////     * @param idPuntoe the idPuntoe to set
////     */
////    public void setIdPuntoe(TbPuntosentrega idPuntoe) {
////        this.idPuntoe = idPuntoe;
////    }
////    

//    /**
//     * @return the idOperador
//     */
//    public Integer getIdOperador() {
//        return idOperador;
//    }
//
//    /**
//     * @param idOperador the idOperador to set
//     */
//    public void setIdOperador(Integer idOperador) {
//        this.idOperador = idOperador;
//    }
//
//    /**
//     * @return the idPuntoorigen
//     */
//    public Integer getIdPuntoorigen() {
//        return idPuntoorigen;
//    }
//
//    /**
//     * @param idPuntoorigen the idPuntoorigen to set
//     */
//    public void setIdPuntoorigen(Integer idPuntoorigen) {
//        this.idPuntoorigen = idPuntoorigen;
//    }
//
//    /**
//     * @return the idUsu
//     */
//    public Integer getIdUsu() {
//        return idUsu;
//    }
//
//    /**
//     * @param idUsu the idUsu to set
//     */
//    public void setIdUsu(Integer idUsu) {
//        this.idUsu = idUsu;
//    }
//
//    /**
//     * @return the idStatus
//     */
//    public Integer getIdStatus() {
//        return idStatus;
//    }
//
//    /**
//     * @param idStatus the idStatus to set
//     */
//    public void setIdStatus(Integer idStatus) {
//        this.idStatus = idStatus;
//    }
//
//    /**
//     * @return the idPuntoe
//     */
//    public Integer getIdPuntoe() {
//        return idPuntoe;
//    }
//
//    /**
//     * @param idPuntoe the idPuntoe to set
//     */
//    public void setIdPuntoe(Integer idPuntoe) {
//        this.idPuntoe = idPuntoe;
//    }

    /**
     * @return the idOperador
     */
    public TbUsuarios getIdOperador() {
        return idOperador;
    }

    /**
     * @param idOperador the idOperador to set
     */
    public void setIdOperador(TbUsuarios idOperador) {
        this.idOperador = idOperador;
    }

    /**
     * @return the idPuntoorigen
     */
    public TbPuntoEo getIdPuntoorigen() {
        return idPuntoorigen;
    }

    /**
     * @param idPuntoorigen the idPuntoorigen to set
     */
    public void setIdPuntoorigen(TbPuntoEo idPuntoorigen) {
        this.idPuntoorigen = idPuntoorigen;
    }

    /**
     * @return the idUsu
     */
    public TbUsuarios getIdUsu() {
        return idUsu;
    }

    /**
     * @param idUsu the idUsu to set
     */
    public void setIdUsu(TbUsuarios idUsu) {
        this.idUsu = idUsu;
    }

    /**
     * @return the idStatus
     */
    public TbStatus getIdStatus() {
        return idStatus;
    }

    /**
     * @param idStatus the idStatus to set
     */
    public void setIdStatus(TbStatus idStatus) {
        this.idStatus = idStatus;
    }

    /**
     * @return the idPuntoe
     */
    public TbPuntoEo getIdPuntoe() {
        return idPuntoe;
    }

    /**
     * @param idPuntoe the idPuntoe to set
     */
    public void setIdPuntoe(TbPuntoEo idPuntoe) {
        this.idPuntoe = idPuntoe;
    }

    /**
     * @return the idRuta
     */
    public TbRuta getIdRuta() {
        return idRuta;
    }

    /**
     * @param idRuta the idRuta to set
     */
    public void setIdRuta(TbRuta idRuta) {
        this.idRuta = idRuta;
    }

    /**
     * @return the idEmpresa
     */
    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    /**
     * @param idEmpresa the idEmpresa to set
     */
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

//    /**
//     * @return the vehiculos
//     */
//    public Set<TbVehiculo> getVehiculos() {
//        return vehiculos;
//    }
//
//    /**
//     * @param vehiculos the vehiculos to set
//     */
//    public void setVehiculos(Set<TbVehiculo> vehiculos) {
//        this.vehiculos = vehiculos;
//    }

    /**
     * @return the idVehiculo
     */
    public TbVehiculo getIdVehiculo() {
        return idVehiculo;
    }

    /**
     * @param idVehiculo the idVehiculo to set
     */
    public void setIdVehiculo(TbVehiculo idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    /**
     * @return the noTarimas
     */
    public Integer getNoTarimas() {
        return noTarimas;
    }

    /**
     * @param noTarimas the noTarimas to set
     */
    public void setNoTarimas(Integer noTarimas) {
        this.noTarimas = noTarimas;
    }

    /**
     * @return the fase
     */
    public Integer getFase() {
        return fase;
    }

    /**
     * @param fase the fase to set
     */
    public void setFase(Integer fase) {
        this.fase = fase;
    }

    /**
     * @return the cita
     */
    public Integer getCita() {
        return cita;
    }

    /**
     * @param cita the cita to set
     */
    public void setCita(Integer cita) {
        this.cita = cita;
    }

    /**
     * @return the noCajas
     */
    public Integer getNoCajas() {
        return noCajas;
    }

    /**
     * @param noCajas the noCajas to set
     */
    public void setNoCajas(Integer noCajas) {
        this.noCajas = noCajas;
    }

}
