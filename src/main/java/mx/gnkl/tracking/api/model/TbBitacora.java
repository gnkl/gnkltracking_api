/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_bitacora")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbBitacora.findAll", query = "SELECT t FROM TbBitacora t"),
    @NamedQuery(name = "TbBitacora.findById", query = "SELECT t FROM TbBitacora t WHERE t.id = :id"),
    @NamedQuery(name = "TbBitacora.findByFecha", query = "SELECT t FROM TbBitacora t WHERE t.fecha = :fecha")})
public class TbBitacora implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Column(name = "nombre")
    @Basic(optional = false)    
    private String nombre;     
    
    @Basic(optional = true)
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
        
    @JoinColumn(name = "pto_destino", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbPuntoEo ptoDestino;
    
    @JoinColumn(name = "pto_origen", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbPuntoEo ptoOrigen;
    
    @JoinColumn(name = "id_vehiculo", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbVehiculo idVehiculo;
    
    @JoinColumn(name = "id_operador", referencedColumnName = "id_usu")
    @ManyToOne(optional = false)
    private TbUsuarios idOperador;

    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usu")
    @ManyToOne(optional = false)
    private TbUsuarios idUsuario;
    
    @JoinColumn(name = "id_status", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbBitacoraStatus idStatus;
    
    @Column(name = "km_inicial")
    private Integer kmInicial;

    @Column(name = "km_final")
    private Integer kmFinal;    
    
    @Column(name = "hora_salida")
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)    
    private Date horaSalida;    

    @Column(name = "hora_llegada")
    @Basic(optional = true)
    @Temporal(TemporalType.TIMESTAMP)    
    private Date horaLlegada;  
    
    @JoinColumn(name = "id_envio", referencedColumnName = "id_envio")
    @ManyToOne(optional = false)
    private TbTipoenvio idEnvio;

    @Column(name = "fecha_creacion")
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)    
    private Date fechaCreacion;  
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "tb_archivo_bitacora", joinColumns = { 
                    @JoinColumn(name = "id_bitacora", nullable = false, updatable = false) }, 
                    inverseJoinColumns = { @JoinColumn(name = "id_archivo", 
                                    nullable = false, updatable = false) })   
    private Set<TbArchivos> archivos;    
    
    @JoinColumn(name = "id_ruta", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbRuta idRuta;    
    
    @Column(name = "is_final")
    private Integer isFinal;

    @Basic(optional = true)
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    
    @Basic(optional = false)
    @Column(name = "is_pausa")
    private Integer isPausa;    

    @Basic(optional = false)
    @Column(name = "activo")
    private Integer activo;
    
    public TbBitacora() {
    }

    public TbBitacora(Integer id) {
        this.id = id;
    }

    public TbBitacora(Integer id, Date fecha) {
        this.id = id;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TbPuntoEo getPtoDestino() {
        return ptoDestino;
    }

    public void setPtoDestino(TbPuntoEo ptoDestino) {
        this.ptoDestino = ptoDestino;
    }

    public TbPuntoEo getPtoOrigen() {
        return ptoOrigen;
    }

    public void setPtoOrigen(TbPuntoEo ptoOrigen) {
        this.ptoOrigen = ptoOrigen;
    }

    public TbVehiculo getIdVehiculo() {
        return idVehiculo;
    }

    public void setIdVehiculo(TbVehiculo idVehiculo) {
        this.idVehiculo = idVehiculo;
    }

    public TbUsuarios getIdOperador() {
        return idOperador;
    }

    public void setIdOperador(TbUsuarios idOperador) {
        this.idOperador = idOperador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbBitacora)) {
            return false;
        }
        TbBitacora other = (TbBitacora) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbBitacora[ id=" + id + " ]";
    }

    /**
     * @return the idUsuario
     */
    public TbUsuarios getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(TbUsuarios idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the idStatus
     */
    public TbBitacoraStatus getIdStatus() {
        return idStatus;
    }

    /**
     * @param idStatus the idStatus to set
     */
    public void setIdStatus(TbBitacoraStatus idStatus) {
        this.idStatus = idStatus;
    }

    /**
     * @return the kmInicial
     */
    public Integer getKmInicial() {
        return kmInicial;
    }

    /**
     * @param kmInicial the kmInicial to set
     */
    public void setKmInicial(Integer kmInicial) {
        this.kmInicial = kmInicial;
    }

    /**
     * @return the kmFinal
     */
    public Integer getKmFinal() {
        return kmFinal;
    }

    /**
     * @param kmFinal the kmFinal to set
     */
    public void setKmFinal(Integer kmFinal) {
        this.kmFinal = kmFinal;
    }

    /**
     * @return the horaSalida
     */
    public Date getHoraSalida() {
        return horaSalida;
    }

    /**
     * @param horaSalida the horaSalida to set
     */
    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    /**
     * @return the horaLlegada
     */
    public Date getHoraLlegada() {
        return horaLlegada;
    }

    /**
     * @param horaLlegada the horaLlegada to set
     */
    public void setHoraLlegada(Date horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    /**
     * @return the idEnvio
     */
    public TbTipoenvio getIdEnvio() {
        return idEnvio;
    }

    /**
     * @param idEnvio the idEnvio to set
     */
    public void setIdEnvio(TbTipoenvio idEnvio) {
        this.idEnvio = idEnvio;
    }

    /**
     * @return the fechaCreacion
     */
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * @param fechaCreacion the fechaCreacion to set
     */
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    /**
     * @return the archivos
     */
    public Set<TbArchivos> getArchivos() {
        return archivos;
    }

    /**
     * @param archivos the archivos to set
     */
    public void setArchivos(Set<TbArchivos> archivos) {
        this.archivos = archivos;
    }    

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the idRuta
     */
    public TbRuta getIdRuta() {
        return idRuta;
    }

    /**
     * @param idRuta the idRuta to set
     */
    public void setIdRuta(TbRuta idRuta) {
        this.idRuta = idRuta;
    }

    /**
     * @return the isFinal
     */
    public Integer getIsFinal() {
        return isFinal;
    }

    /**
     * @param isFinal the isFinal to set
     */
    public void setIsFinal(Integer isFinal) {
        this.isFinal = isFinal;
    }

    /**
     * @return the fechaFin
     */
    public Date getFechaFin() {
        return fechaFin;
    }

    /**
     * @param fechaFin the fechaFin to set
     */
    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    /**
     * @return the isPausa
     */
    public Integer getIsPausa() {
        return isPausa;
    }

    /**
     * @param isPausa the isPausa to set
     */
    public void setIsPausa(Integer isPausa) {
        this.isPausa = isPausa;
    }

    /**
     * @return the activo
     */
    public Integer getActivo() {
        return activo;
    }

    /**
     * @param activo the activo to set
     */
    public void setActivo(Integer activo) {
        this.activo = activo;
    }
    
}
