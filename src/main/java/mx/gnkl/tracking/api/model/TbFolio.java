/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_folio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbFolio.findAll", query = "SELECT t FROM TbFolio t"),
    @NamedQuery(name = "TbFolio.findByFolioNovartis", query = "SELECT t FROM TbFolio t WHERE t.folioNovartis = :folioNovartis"),
    @NamedQuery(name = "TbFolio.findByFolioJaloma", query = "SELECT t FROM TbFolio t WHERE t.folioJaloma = :folioJaloma"),
    @NamedQuery(name = "TbFolio.findByFolioDegasa", query = "SELECT t FROM TbFolio t WHERE t.folioDegasa = :folioDegasa")})
public class TbFolio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "folio_novartis")
    private Long folioNovartis;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "folio_jaloma")
    private Long folioJaloma;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "folio_degasa")
    private Long folioDegasa;

    public TbFolio() {
    }

    public TbFolio(Long folioNovartis) {
        this.folioNovartis = folioNovartis;
    }

    public TbFolio(Long folioNovartis, Long folioJaloma, Long folioDegasa) {
        this.folioNovartis = folioNovartis;
        this.folioJaloma = folioJaloma;
        this.folioDegasa = folioDegasa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getFolioNovartis() != null ? getFolioNovartis().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbFolio)) {
            return false;
        }
        TbFolio other = (TbFolio) object;
        if ((this.getFolioNovartis() == null && other.getFolioNovartis() != null) || (this.getFolioNovartis() != null && !this.folioNovartis.equals(other.folioNovartis))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbFolio[ folioNovartis=" + getFolioNovartis() + " ]";
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the folioNovartis
     */
    public Long getFolioNovartis() {
        return folioNovartis;
    }

    /**
     * @param folioNovartis the folioNovartis to set
     */
    public void setFolioNovartis(Long folioNovartis) {
        this.folioNovartis = folioNovartis;
    }

    /**
     * @return the folioJaloma
     */
    public Long getFolioJaloma() {
        return folioJaloma;
    }

    /**
     * @param folioJaloma the folioJaloma to set
     */
    public void setFolioJaloma(Long folioJaloma) {
        this.folioJaloma = folioJaloma;
    }

    /**
     * @return the folioDegasa
     */
    public Long getFolioDegasa() {
        return folioDegasa;
    }

    /**
     * @param folioDegasa the folioDegasa to set
     */
    public void setFolioDegasa(Long folioDegasa) {
        this.folioDegasa = folioDegasa;
    }
    
}
