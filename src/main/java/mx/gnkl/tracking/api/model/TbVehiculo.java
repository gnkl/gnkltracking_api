/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_vehiculo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbVehiculo.findAll", query = "SELECT t FROM TbVehiculo t"),
    @NamedQuery(name = "TbVehiculo.findById", query = "SELECT t FROM TbVehiculo t WHERE t.id = :id"),
    @NamedQuery(name = "TbVehiculo.findByNoEconomico", query = "SELECT t FROM TbVehiculo t WHERE t.noEconomico = :noEconomico"),
    @NamedQuery(name = "TbVehiculo.findByDescripcion", query = "SELECT t FROM TbVehiculo t WHERE t.descripcion = :descripcion")})
public class TbVehiculo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "no_economico")
    private String noEconomico;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "placas")
    private String placas;
    @Column(name = "no_serie")
    private String noSerie;
    @Column(name = "marca")
    private String marca;
    @Column(name = "submarca")
    private String submarca;
    @Column(name = "cap_carga")
    private Integer capCarga;
    
    public TbVehiculo() {
    }

    public TbVehiculo(Integer id) {
        this.id = id;
    }

    public TbVehiculo(Integer id, String noEconomico) {
        this.id = id;
        this.noEconomico = noEconomico;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoEconomico() {
        return noEconomico;
    }

    public void setNoEconomico(String noEconomico) {
        this.noEconomico = noEconomico;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbVehiculo)) {
            return false;
        }
        TbVehiculo other = (TbVehiculo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbVehiculo[ id=" + id + " ]";
    }

    /**
     * @return the placas
     */
    public String getPlacas() {
        return placas;
    }

    /**
     * @param placas the placas to set
     */
    public void setPlacas(String placas) {
        this.placas = placas;
    }

    /**
     * @return the noSerie
     */
    public String getNoSerie() {
        return noSerie;
    }

    /**
     * @param noSerie the noSerie to set
     */
    public void setNoSerie(String noSerie) {
        this.noSerie = noSerie;
    }

    /**
     * @return the marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * @param marca the marca to set
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * @return the submarca
     */
    public String getSubmarca() {
        return submarca;
    }

    /**
     * @param submarca the submarca to set
     */
    public void setSubmarca(String submarca) {
        this.submarca = submarca;
    }

    /**
     * @return the capCarga
     */
    public Integer getCapCarga() {
        return capCarga;
    }

    /**
     * @param capCarga the capCarga to set
     */
    public void setCapCarga(Integer capCarga) {
        this.capCarga = capCarga;
    }
    
}
