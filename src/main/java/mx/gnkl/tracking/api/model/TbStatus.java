/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbStatus.findAll", query = "SELECT t FROM TbStatus t"),
    @NamedQuery(name = "TbStatus.findByIdStatus", query = "SELECT t FROM TbStatus t WHERE t.idStatus = :idStatus"),
    @NamedQuery(name = "TbStatus.findByDesStatus", query = "SELECT t FROM TbStatus t WHERE t.desStatus = :desStatus")})
public class TbStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_status")
    private Integer idStatus;
    @Column(name = "num_status")
    private Integer numStatus;
    @Size(max = 50)
    @Column(name = "des_status")
    private String desStatus;
    @JoinColumn(name = "id_envio", referencedColumnName = "id_envio")
    @ManyToOne(optional = false)
    private TbTipoenvio idEnvio;
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Column(name = "comentario")
    private String comentario;
    
    public TbStatus() {
    }

    public TbStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public Integer getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Integer idStatus) {
        this.idStatus = idStatus;
    }

    public String getDesStatus() {
        return desStatus;
    }

    public void setDesStatus(String desStatus) {
        this.desStatus = desStatus;
    }

//    @XmlTransient
//    public Set<TbPaquetes> getTbPaquetesSet() {
//        return tbPaquetesSet;
//    }
//
//    public void setTbPaquetesSet(Set<TbPaquetes> tbPaquetesSet) {
//        this.tbPaquetesSet = tbPaquetesSet;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idStatus != null ? idStatus.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbStatus)) {
            return false;
        }
        TbStatus other = (TbStatus) object;
        if ((this.idStatus == null && other.idStatus != null) || (this.idStatus != null && !this.idStatus.equals(other.idStatus))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbStatus[ idStatus=" + idStatus + " ]";
    }

    /**
     * @return the idEnvio
     */
    public TbTipoenvio getIdEnvio() {
        return idEnvio;
    }

    /**
     * @param idEnvio the idEnvio to set
     */
    public void setIdEnvio(TbTipoenvio idEnvio) {
        this.idEnvio = idEnvio;
    }

    /**
     * @return the numStatus
     */
    public Integer getNumStatus() {
        return numStatus;
    }

    /**
     * @param numStatus the numStatus to set
     */
    public void setNumStatus(Integer numStatus) {
        this.numStatus = numStatus;
    }

    /**
     * @return the idEmpresa
     */
    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    /**
     * @param idEmpresa the idEmpresa to set
     */
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    /**
     * @return the comentario
     */
    public String getComentario() {
        return comentario;
    }

    /**
     * @param comentario the comentario to set
     */
    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
}
