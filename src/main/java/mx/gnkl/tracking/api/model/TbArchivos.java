/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_archivos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbArchivos.findAll", query = "SELECT t FROM TbArchivos t"),
    @NamedQuery(name = "TbArchivos.findByIdArchivo", query = "SELECT t FROM TbArchivos t WHERE t.idArchivo = :idArchivo"),
    @NamedQuery(name = "TbArchivos.findByPathArchivo", query = "SELECT t FROM TbArchivos t WHERE t.pathArchivo = :pathArchivo")})
public class TbArchivos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_archivo")
    private Integer idArchivo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "path_archivo")
    private String pathArchivo;

    public TbArchivos() {
    }

    public TbArchivos(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public TbArchivos(Integer idArchivo, String pathArchivo) {
        this.idArchivo = idArchivo;
        this.pathArchivo = pathArchivo;
    }

    public Integer getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(Integer idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String getPathArchivo() {
        return pathArchivo;
    }

    public void setPathArchivo(String pathArchivo) {
        this.pathArchivo = pathArchivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArchivo != null ? idArchivo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbArchivos)) {
            return false;
        }
        TbArchivos other = (TbArchivos) object;
        if ((this.idArchivo == null && other.idArchivo != null) || (this.idArchivo != null && !this.idArchivo.equals(other.idArchivo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbArchivos[ idArchivo=" + idArchivo + " ]";
    }
    
}
