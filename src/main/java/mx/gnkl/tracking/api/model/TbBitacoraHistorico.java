/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gnkl.tracking.api.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_bitacora_historico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbBitacoraHistorico.findAll", query = "SELECT t FROM TbBitacoraHistorico t"),
    @NamedQuery(name = "TbBitacoraHistorico.findById", query = "SELECT t FROM TbBitacoraHistorico t WHERE t.id = :id"),
    @NamedQuery(name = "TbBitacoraHistorico.findByFecha", query = "SELECT t FROM TbBitacoraHistorico t WHERE t.fecha = :fecha")})
public class TbBitacoraHistorico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @JoinColumn(name = "estatus", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbBitacoraStatus estatus;
    
    @JoinColumn(name = "id_bitacora", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbBitacora idBitacora;
    
    @Column(name = "kilometraje")
    private Integer kilometraje;

    @Column(name = "observaciones")
    private String observaciones;
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "tb_archivo_hist", joinColumns = { 
                    @JoinColumn(name = "id_historico", nullable = false, updatable = false) }, 
                    inverseJoinColumns = { @JoinColumn(name = "id_archivo", 
                                    nullable = false, updatable = false) })   
    private Set<TbArchivos> archivos;    
    
    public TbBitacoraHistorico() {
    }

    public TbBitacoraHistorico(Integer id) {
        this.id = id;
    }

    public TbBitacoraHistorico(Integer id, Date fecha) {
        this.id = id;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TbBitacoraStatus getEstatus() {
        return estatus;
    }

    public void setEstatus(TbBitacoraStatus estatus) {
        this.estatus = estatus;
    }

    public TbBitacora getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(TbBitacora idBitacora) {
        this.idBitacora = idBitacora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbBitacoraHistorico)) {
            return false;
        }
        TbBitacoraHistorico other = (TbBitacoraHistorico) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbBitacoraHistorico[ id=" + id + " ]";
    }

    /**
     * @return the kilometraje
     */
    public Integer getKilometraje() {
        return kilometraje;
    }

    /**
     * @param kilometraje the kilometraje to set
     */
    public void setKilometraje(Integer kilometraje) {
        this.kilometraje = kilometraje;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    /**
     * @return the archivos
     */
    public Set<TbArchivos> getArchivos() {
        return archivos;
    }

    /**
     * @param archivos the archivos to set
     */
    public void setArchivos(Set<TbArchivos> archivos) {
        this.archivos = archivos;
    }
    
}
